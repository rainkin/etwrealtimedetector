import json

if __name__ == "__main__":
    inFile = open("111.txt","r");
    outFile = open("1111.txt","w");
    uniqueSyscall = set()

    sig = inFile.read();
    for eachSig in json.loads(sig):
        for eachSys in eachSig["sig"]:
            uniqueSyscall.add(eachSys)

    for eachSys in uniqueSyscall:
        outFile.write(eachSys+"\n")
