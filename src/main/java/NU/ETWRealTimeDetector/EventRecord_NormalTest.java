package NU.ETWRealTimeDetector;

/**
 * This EventRecord_NormalTest is the event record class most used and general in real-time detector project.
 * It is exactly which event record class is used in the future detector deployment or production.
 * @author ander
 * originalOrder: the index of this record in the original trace received by the consumer.
 * absoluteTime: the reception time of the message where this event record belongs to.
 * eventType: the event type. ONE of the most TWO USEFUL fields in this class.
 * arguments: Temporarily of no use. It is designed to represent non-string parameters.
 * opcode: Temporarily of no use. It is defined by Chunlin.
 * tid: the thread id of this event record.
 * pid: the process id of this event record.
 * parameter: String parameter. ONE of the more TWO USEFUL fields in this class.
 * order: the index of this record in the pre-processed trace when detect() triggered in the consumer.
 * pcid: the id of the client host of this event record.
 */

public class EventRecord_NormalTest {
	public Integer originalOrder;
	public String absoluteTime;
	public String eventType;
	public String arguments;
	public String opcode;
	public String tid;
	public String pid;
	public String parameter;
	public Integer order;
	public String pcid;
	
	public EventRecord_NormalTest(Integer originalOrder, String absoluteTime, String eventType, String arguments, String opcode, String tid, 
			   String pid, String parameter, Integer order, String pcid) {
		this.originalOrder = originalOrder;
		this.absoluteTime = absoluteTime;
		this.eventType = eventType;
		this.arguments = arguments;
		this.opcode = opcode;
		this.tid = tid;
		this.pid = pid;
		this.parameter = parameter;
		this.order = order;
		this.pcid = pcid;
	}
	
	public EventRecord_NormalTest(EventRecord_NormalTest target) {
		this.originalOrder = target.originalOrder;
		this.absoluteTime = target.absoluteTime;
		this.eventType = target.eventType;
		this.arguments = target.arguments;
		this.opcode = target.opcode;
		this.tid = target.tid;
		this.pid = target.pid;
		this.parameter = target.parameter;
		this.order = target.order;
		this.pcid = target.pcid;
	}
	
	public EventRecord_NormalTest() {
		// TODO Auto-generated constructor stub
	}
	
	@Override public boolean equals(Object anObject) {
		if (anObject instanceof EventRecord_NormalTest) 
			if (((EventRecord_NormalTest)anObject).eventType.equals(this.eventType) && ((EventRecord_NormalTest)anObject).parameter.equals(this.parameter))
				return true;
		return false;
	}
	
	@Override public int hashCode() {
		if (parameter != null)
			return (41 * (eventType.hashCode() + 41) + parameter.hashCode());
		else
			return (41 * (eventType.hashCode() + 41));
	}
	
	public String getPid(){
		return this.pid;
	}
	
	public String getThreadId() {
		return this.tid;
	}
	
}
