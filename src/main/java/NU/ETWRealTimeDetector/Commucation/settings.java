package NU.ETWRealTimeDetector.Commucation;


/**
 * 
 *
 * @File_name:
 * @author Liheng Xu
 * @Date:17-12-15
 * @Description:���ó����ĵ�
 * @Others:
 * @Function_List: settings,Activemq_Setting
 * @Version: 0.8
 * @History:
 *
 */

public class settings {
	
	/**active����*/
	//activemq ��������
	protected static String host_url="failover:(tcp://127.0.0.1:61616)";
	//activemq �������û���
	protected static String activemq_user="admin";
	//activemq ����������
	protected static String activemq_password="admin";
	//�Ƿ񱣴�activemq�Ĵ����ļ�������
	protected static boolean NeedDump=false;
	//��������detector��ͨ����
	protected static String responce_detection_dest="detector_res";
	//detector �������������ͨ����
	protected static String detection_dest="detector";
	
	
	
	/**req����**/
	//client �ر�����
	protected static final String StopDet="Stop";
	//client ��������
	protected static final String InitDet="Init";
	protected static final String Process="Process";
	protected static final String WhiteListUpdate="WhiteList";
	//������ ��������
	protected static final String BlackListUpdate="BlackList";
	protected static final String BlackList="blacklist";
	protected static final String WhiteList="whitelist";
	protected static final String ip="machine_ip";
	protected static final String content="content";
	protected static final String machine_name="uuid";
	protected static final String command="command_id";
	protected static final String version="version";
	protected static final String description="description";
	protected static final String start_date="Start_Date";
	protected static final String end_date="End_Date";
	protected static final String lastest_version_url="Lastest_Version_Url";
	protected static final String success="Success";
	protected static final String fail="Fail";
	protected static final String log="Log";
	//��ͻ���������־
	protected static final String ask_for_log="Ask_For_Log";
	protected static final String update_blacklist="Update_Blacklist";
	//�ͻ��˸���
	protected static final String update_client="Update_Client";
	//������������
	protected static final String process_create="Process_Create";
	//���̽�������
	protected static final String process_end="Process_End";
	protected static final String processid="pid";
	//��ͻ��˷���ɱ����������
	protected static final String kill_process="Kill_Process";
	//�߳����� ����
	protected static final String thread_create="Thread_Create";
	//�̹߳ر� ����
	protected static final String thread_end="Thread_End";
	protected static final String threadid="tid";
	//detector����report��sch
	protected static final String report="Report";
	//����md5����
	protected static final String rec_MD5="Rec_MD5";
	//����md5����
	protected static final String req_MD5="Req_MD5";
	//���Ӻ�����
	protected static final String addBlacklist="addBlacklist";
	//���Ӱ�����
	protected static final String addWhitelist = "addWhitelist";
	//ɾ��������
	protected static final String delblacklist = "delBlacklist";
	//ɾ��������
	protected static final String delWhitelist = "delWhitelist";
	//�Ӻ������Ƶ�������
	protected static final String blacktowhite = "BlacktoWhite";
	//�Ӱ������Ƶ�������
	protected static final String whitetoblack = "WhitetoBlack";
	protected static final String revevant_id = "related_id";
	protected static final String phf = "phf";
	protected static final String WebPortal = "webportal";
	protected static final String confictblacklist = "confictBlacklist";
	protected static final String confictwhitelist = "confictWhitelist"; 
	protected static final String absolute_path = "absolute_path"; 
	protected static final String APP_MD5="app_md5";
	protected static final String TIME_STAMP = "time_stamp";
	protected static final String commandline = "commandline";
	//�ڲ����� �ͷ�processmap��ָ��client����Դ
	protected static final String processmap_del_client = "processmap_del_client";
	//�ڲ����� ����processmap��ָ��client
	protected static final String processmap_init_client = "processmap_init_client";
	protected static final String process_id="process_id";
	
}


class Activemq_Setting{
	public String url=new String();
	public String user=new String();
	public String password=new String();
	public String dest=new String();
	public Activemq_Setting(String User,String Password, String Url, String Dest) {
		this.url=Url;
		this.user=User;
		this.dest=Dest;
		this.password=Password;
	}
}
