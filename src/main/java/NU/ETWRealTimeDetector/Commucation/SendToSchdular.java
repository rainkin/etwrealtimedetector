package NU.ETWRealTimeDetector.Commucation;

/**
 * 
 *
 * @File_name:
 * @author Liheng Xu
 * @Date: 17-12-20
 * @Description: ʹ��ǰ��Ҫnew����SendToSchdular(String machinename)��Ҫ����detector��Ӧ�ͻ��˵�machine_name��activemqʹ��Ĭ������
 * SendToSchdular(String machinename,String user,String pass,String url,String dest)����Ҫ�Զ���activemq����
 * ��Ҫ������Ϣ���ö�Ӧģ�鲢�����������
 * @Others:
 * @Function_List:
 * @Version:
 * @History:
 *
 */

public class SendToSchdular {
	Activemq_Setting Res;
	private String machine_name;
	private jms_activemq_SendMessagesToSchdular SendMessagesToSchdular;
	
	public SendToSchdular(String machinename) {
		// TODO Auto-generated constructor stub
		Res.dest=settings.detection_dest;
		Res.password=settings.activemq_password;
		Res.url=settings.host_url;
		Res.user=settings.activemq_user;
		this.machine_name=machinename;
		this.SendMessagesToSchdular=new jms_activemq_SendMessagesToSchdular(this.Res);
	}
	
	public SendToSchdular(String machinename,String user,String pass,String url,String dest) {
		this.machine_name=machinename;
		this.Res.dest=dest;
		this.Res.url=url;
		this.Res.user=user;
		this.Res.password=pass;
		this.SendMessagesToSchdular=new jms_activemq_SendMessagesToSchdular(this.Res);
	}
	
	public void SendReportToSchdular(int tid,int pid,String phf) {
		SendMessagesToSchdular.send_message(CreateJson.Create_json(machine_name, settings.report,CreateJson.Create_content_json(settings.threadid, settings.processid, settings.phf, 
				Integer.toString(tid), Integer.toString(pid), phf)));
	}
	
	public void SendProcessCreateToSchdular(int pid,String path,String commandline) {
		SendMessagesToSchdular.send_message(CreateJson.Create_json(machine_name, settings.process_create,CreateJson.Create_content_json(settings.processid, settings.absolute_path, settings.commandline, 
				Integer.toString(pid), path, commandline)));
	}
	
	public void SendProcessEndToSchdular(int pid) {
		SendMessagesToSchdular.send_message(CreateJson.Create_json(machine_name, settings.process_end,CreateJson.Create_content_json(settings.processid,
				Integer.toString(pid))));
	}
	
	public void SendThreadCreateToSchdular(int tid,int pid) {
		SendMessagesToSchdular.send_message(CreateJson.Create_json(machine_name, settings.thread_create,CreateJson.Create_content_json(settings.threadid, settings.processid, 
				Integer.toString(tid), Integer.toString(pid))));
	}
	
	public void SendThreadEndToSchdular(int tid) {
		SendMessagesToSchdular.send_message(CreateJson.Create_json(machine_name, settings.thread_end,CreateJson.Create_content_json(settings.threadid,
				Integer.toString(tid))));
	}
}
