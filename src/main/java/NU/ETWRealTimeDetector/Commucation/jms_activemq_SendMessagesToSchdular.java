package NU.ETWRealTimeDetector.Commucation;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 * 
 *
 * @File_name:
 * @author Liheng Xu
 * @Date: 17-12-20
 * @Description: 
 * @Others:
 * @Function_List:
 * @Version:
 * @History:
 *
 */
public class jms_activemq_SendMessagesToSchdular {
	private ConnectionFactory connectionFactory=null; // 连接工厂
    private Connection connection = null;
    private Session session = null;
    private Destination destination=null; // 消息的目的地
    private MessageProducer producer;
    private Activemq_Setting Res;

	private void close() throws JMSException {    
        if (connection != null) {    
            connection.close();    
        }    
    }
    
    public jms_activemq_SendMessagesToSchdular(Activemq_Setting Res) {
    	try {
			response_init(Res);
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			System.out.println("Init response fail.");
		}
    }
    
	public void response_init(Activemq_Setting res) throws JMSException{
		System.out.printf("%s %s %s\n",res.user,res.password,res.url);
		connectionFactory=new ActiveMQConnectionFactory(res.user,res.password,res.url);
		connection=connectionFactory.createConnection(); // 通过连接工厂获取连接
        try {  
            connection.start();
        } catch (JMSException e) {
			connection.close();
            e.printStackTrace();        
            System.out.printf("创建%s失败",res.dest);
        }
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);  //创建session 不带事务  
        destination = session.createTopic(res.dest); //创建topic  
        producer = session.createProducer(destination);    //创建publisher  
        Res=res;
        System.out.printf("创建%s连接",res.dest);
	}
	
	public void send_message(String message) {
		try {
			if (connection==null) {
				response_init(Res);
			}
			TextMessage Message=session.createTextMessage(message);
			producer.send(Message);
		}catch(JMSException e) {
			e.printStackTrace();
		}
	}
}