package NU.ETWRealTimeDetector.Commucation;

import com.google.gson.JsonObject;

/**
 * 
 *
 * @File_name:
 * @author Liheng Xu
 * @Date:2017��12��20��
 * @Description:
 * @Others:
 * @Function_List:
 * @Version:
 * @History:
 *
 */

public class CreateJson {
	
	static JsonObject Create_content_json(String name1,String value1) {
		JsonObject json = new JsonObject();
		json.addProperty(name1, value1);
		return json;
	}
	
	static JsonObject Create_content_json(String name1,String name2,String value1,String value2) {
		JsonObject json = new JsonObject();
		json.addProperty(name1, value1);
		json.addProperty(name2, value2);
		return json;
	}
	
	static JsonObject Create_content_json(String name1,String name2,String name3,String value1,String value2,String value3) {
		JsonObject json = new JsonObject();
		json.addProperty(name1, value1);
		json.addProperty(name2, value2);
		json.addProperty(name3, value3);
		return json;
	}

	static String Create_json(String machine_name,String command,JsonObject content) {
		JsonObject json = new JsonObject();
		json.addProperty(settings.machine_name, machine_name);
		json.addProperty(settings.command, command);
		json.add(settings.content, content);
		return json.toString();
	}

}
