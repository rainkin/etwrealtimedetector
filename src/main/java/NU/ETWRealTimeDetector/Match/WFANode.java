package NU.ETWRealTimeDetector.Match;

import java.util.Arrays;

public class WFANode {
	WFANode[] succ;
	WFANode pred;
	int pos;
	int originalPos;
	String absoluteTime;
	
	WFANode(int scrcnt) {
		succ = new WFANode[scrcnt];
		pred = null;
		pos = -1;
	}
}
