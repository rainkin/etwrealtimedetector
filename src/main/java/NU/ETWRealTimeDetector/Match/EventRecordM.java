package NU.ETWRealTimeDetector.Match;

public class EventRecordM {
	public String name;
	public String parameter;
	public String absoluteTime;
	public int originalPos;
	
	public EventRecordM(String name, String parameter) {
		this.name = name;
		this.parameter = parameter;
	}
	
	@Override public boolean equals(Object anObject) {
		if (anObject instanceof EventRecordM) 
			if (((EventRecordM)anObject).name.equals(this.name))
				if (((EventRecordM)anObject).parameter == this.parameter ||
					((EventRecordM)anObject).parameter.equals(this.parameter))
				return true;
		return false;
	}
	
	@Override public int hashCode() {
		if (parameter != null)
			return (41 * (name.hashCode() + 41) + parameter.hashCode());
		else
			return (41 * (name.hashCode() + 41));
	}
}
