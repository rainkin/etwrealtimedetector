package NU.ETWRealTimeDetector.Match;

public class TraceKey {
	public String tid;
	public String pid;
	public String pcid;
	
	public TraceKey() {
		
	}
	
	public TraceKey(String threadid, String pid, String pcid) {
		this.tid = threadid;
		this.pcid = pcid;
		this.pid = pid;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pcid == null) ? 0 : pcid.hashCode());
		result = prime * result + ((pid == null) ? 0 : pid.hashCode());
		result = prime * result + ((tid == null) ? 0 : tid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TraceKey other = (TraceKey) obj;
		if (pcid == null) {
			if (other.pcid != null)
				return false;
		} else if (!pcid.equals(other.pcid))
			return false;
		if (pid == null) {
			if (other.pid != null)
				return false;
		} else if (!pid.equals(other.pid))
			return false;
		if (tid == null) {
			if (other.tid != null)
				return false;
		} else if (!tid.equals(other.tid))
			return false;
		return true;
	}
}
