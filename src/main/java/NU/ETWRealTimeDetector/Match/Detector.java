package NU.ETWRealTimeDetector.Match;

import NU.ETWRealTimeDetector.EventRecord_NormalTest;
import NU.ETWRealTimeDetector.Commucation.SendToSchdular;
import NU.ETWRealTimeDetector.Output.Publisher;

public abstract class Detector {
	
	protected Publisher publisher;
	protected SendToSchdular communication;

	public abstract void inputPHF(EventRecord_NormalTest record);
	
	public abstract void inputInit(EventRecord_NormalTest record);
	
	public abstract void detect(boolean forPhf);

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
	
	public void setCommunication(SendToSchdular communication) {
		this.communication = communication;
	}
}
