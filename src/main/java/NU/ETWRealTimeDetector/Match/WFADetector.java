package NU.ETWRealTimeDetector.Match;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import NU.ETWRealTimeDetector.EventRecord_NormalTest;
import NU.ETWRealTimeDetector.Commucation.SendToSchdular;
import NU.ETWRealTimeDetector.Input.ActiveMQConsumer;
import NU.ETWRealTimeDetector.Output.Result;

public class WFADetector extends Detector{
	public TraceKey tk = null;
	public final static int MaxSigCnt = 100;
	public final static int MaxPhfCnt = 5;
	public final static int MaxSeqSigLen = 2000;
	/**
	 * PHF list being tested in the detector.
	 */
	public final static String[] phf = {
								"audiorecord",
								"keylogger", 
								"remotedesktop_withoutALPC",
//								"Runqingkeylogger",
							//	"Runqingremotedesktop",
								"remoteshell",
								"initialization"
								//"send&execution"
								//"urldownload"
			  					};
	/**
	 * Credit-based alarming mechanism threshold
	 */
	public static Double[] detectionThreshold = {
			//1.0, 
			1.0, 
			1.0, 
			1.0, 
			1.0, 
			1.0
	//		1.0
			//Double.POSITIVE_INFINITY
			};
	/**
	 * Label for whether it is sequence-based detection for specific phf.
	 */
	public static boolean[] isSequence = {
			true, 
			true, 
			true,
			true,
			false
	//		true,
//			true,
//			false
			//false
			};
	
	/**
	 * Signature related setting and overall data.
	 */
	public static final String signatureFolder = "signatures/12.22/";
	public static final String signatureFileExtention = "_localResults_filtered.txt";
	public static List<SigResults>[] sigResults = null;
	public static Integer[] phfSignatureCnt = new Integer[MaxPhfCnt];
	public static int sequenceSignatureMaxWindowsize = 0;
	public static int setSignatureMaxWindowsize = 0;
	public static Double[][] singleCredit = new Double[MaxPhfCnt][MaxSigCnt];
	public static Integer[][] windowsize = new Integer[MaxPhfCnt][MaxSigCnt];
	
	public boolean[][] sigMatched = new boolean[MaxPhfCnt][MaxSigCnt];
	public boolean[] phfMatched = new boolean[MaxPhfCnt];
	public double[] accumulatedCredit = new double[MaxPhfCnt];
	
	/**
	 * Token-based signature detection data.
	 */
	public static HashMap<EventRecordM, Integer>[][] setSignature = new HashMap[MaxPhfCnt][MaxSigCnt];
	public LinkedList<EventRecordM> traceList = null;
	public HashMap<EventRecordM, Integer> traceSet = null;
	
	/**
	 * Sequence-based signature detection data.
	 */
	public static int uniqueEventCnt = 0;
	public static HashMap<EventRecordM, Integer> mapping = new HashMap<EventRecordM, Integer>();
	public static HashMap<String, HashSet<String>> usefulParameter = new HashMap<String, HashSet<String>>();
	public static List<EventRecordM>[][] sequenceSignature = new List[MaxPhfCnt][MaxSigCnt];
	public WFANode[] firstOccurrenceEvidence = new WFANode[MaxSeqSigLen];
	public WFANode[] last = null;
	public WFANode root = null;
	public LinkedList<WFANode> WFA = null;
	public int detectedSequenceTraceEventCnt = 0;
	public int detectedSetTraceEventCnt = 0;
	
	
	static {
		setSignatureMaxWindowsize = 10000;
		if (sigResults == null) {
			sigResults = new List[MaxPhfCnt];
			for (int phfNo = 0 ; phfNo < MaxPhfCnt; ++phfNo) 
			if (isSequence[phfNo]) {
				/**
				 * Count unique eventRecords & Load sequence signatures
				 * Construct the mapping between eventRecord and Integer as alphabets
				 * (A,"") and (A,"str") were treated as different events
				 */
				try {
					sigResults[phfNo] = new Gson().fromJson(new FileReader(signatureFolder + phf[phfNo] + signatureFileExtention), new TypeToken<List<SigResults>>(){}.getType());
				} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
					e.printStackTrace();
				}
				phfSignatureCnt[phfNo] = sigResults[phfNo].size();
				String[] splitResult;
				for (int i = 0; i < phfSignatureCnt[phfNo]; ++i) {
					windowsize[phfNo][i] = sigResults[phfNo].get(i).windowsize;
					singleCredit[phfNo][i] = sigResults[phfNo].get(i).coverage / sigResults[phfNo].get(i).falsePositiveShare;
					sequenceSignature[phfNo][i] = new ArrayList<EventRecordM>();
					if (windowsize[phfNo][i] > sequenceSignatureMaxWindowsize) sequenceSignatureMaxWindowsize = windowsize[phfNo][i];
					for (int j = 0; j < sigResults[phfNo].get(i).sig.size(); ++j) {
						splitResult = sigResults[phfNo].get(i).sig.get(j).split(" @ ");
						if (splitResult[0].equals("ImageDCStart")) splitResult[0] = "ImageLoad";
						EventRecordM newEventRM = new EventRecordM(splitResult[0], "");
						if (!mapping.containsKey(newEventRM)) {
							mapping.put(newEventRM, (Integer)uniqueEventCnt);
							++uniqueEventCnt;
						}
						if (!usefulParameter.containsKey(splitResult[0])) {
							usefulParameter.put(splitResult[0], new HashSet<String>());
							usefulParameter.get(splitResult[0]).add("");
						}
						/**
						 * The signature eventRecord has parameter.
						 */
						if (splitResult.length > 1 && !splitResult[1].equals("")) {
							EventRecordM newEventRM2 = new EventRecordM(splitResult[0], splitResult[1]);
							usefulParameter.get(splitResult[0]).add(splitResult[1]);
							if (!mapping.containsKey(newEventRM2)) {
								mapping.put(newEventRM2, (Integer)uniqueEventCnt);
								++uniqueEventCnt;
							}
							sequenceSignature[phfNo][i].add(newEventRM2);
						} else
							sequenceSignature[phfNo][i].add(newEventRM);
					}
				}
			} else {
				/**
				 * Load set signatures
				 */
				try {
					sigResults[phfNo] = new Gson().fromJson(new FileReader(signatureFolder + phf[phfNo] + signatureFileExtention), new TypeToken<List<SigResults>>(){}.getType());
				} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
					e.printStackTrace();
				}
				phfSignatureCnt[phfNo] = sigResults[phfNo].size();
				String[] splitResult;
				for (int i = 0; i < phfSignatureCnt[phfNo]; ++i) {
					windowsize[phfNo][i] = sigResults[phfNo].get(i).windowsize;
					singleCredit[phfNo][i] = sigResults[phfNo].get(i).coverage / sigResults[phfNo].get(i).falsePositiveShare;
					setSignature[phfNo][i] = new HashMap<EventRecordM, Integer>();
					if (windowsize[phfNo][i] > setSignatureMaxWindowsize) setSignatureMaxWindowsize = windowsize[phfNo][i];
					for (int j = 0; j < sigResults[phfNo].get(i).sig.size(); ++j) {
						splitResult = sigResults[phfNo].get(i).sig.get(j).split(" @ ");
						if (splitResult[0].equals("ImageDCStart")) splitResult[0] = "ImageLoad";
						EventRecordM newSysRM;
						if (splitResult.length == 1) {
							newSysRM = new EventRecordM(splitResult[0], "");
						} else {
							newSysRM = new EventRecordM(splitResult[0], splitResult[1]);
						}
						if (!setSignature[phfNo][i].containsKey(newSysRM)) {
							setSignature[phfNo][i].put(newSysRM, 1);
						} else {
							setSignature[phfNo][i].put(newSysRM, setSignature[phfNo][i].get(newSysRM) + 1);
						}
					}
				}
			}
		}
		//System.out.println(signatureCnt[0] + " " + signatureCnt[1] + " " + signatureCnt[2]);
	}
	
	public WFADetector(TraceKey tk) {
		this.tk = tk;
		/**
		 * Initialize WFA.
		 */
		WFA = new LinkedList<WFANode>();
		last = new WFANode[uniqueEventCnt + 1];
		root = new WFANode(uniqueEventCnt);
		last[uniqueEventCnt] = root;
		/**
		 * Initialize set detection.
		 */
		traceSet = new HashMap<EventRecordM, Integer>();
		traceList = new LinkedList<EventRecordM>();
	}
	
	/**
	 * Go along on the specific signature(sigNo, phfNo) to check whether it is matched.
	 * @param startNode
	 * @param sigNo
	 * @param phfNo
	 * @param newResult
	 */
	public void testStartAt(WFANode startNode, int sigNo, int phfNo, Result newResult) {
		newResult.pid = tk.pid;
		newResult.tid = tk.tid;
		newResult.pcid = tk.pcid;
		String phfName = phf[phfNo];
		if (isSequence[phfNo]) {
			int sigLen = sequenceSignature[phfNo][sigNo].size();
			Arrays.fill(firstOccurrenceEvidence, null);
			/**
			 * If it is the sequence-based detection. Check along WFA with the specific signature.
			 */
			WFANode cursorNode = startNode;
			for (int k = 0; k < sigLen; ++k) {
				cursorNode = cursorNode.succ[mapping.get(sequenceSignature[phfNo][sigNo].get(k))];
				if (cursorNode == null) break;
				firstOccurrenceEvidence[k] = cursorNode;
			}
			int usedWindowsize = 62500;
			/**
			 * if cursorNode doesn't go outside WFA(become null), one match is found.
			 */
			if (cursorNode != null) {
				usedWindowsize = firstOccurrenceEvidence[sequenceSignature[phfNo][sigNo].size() - 1].pos - firstOccurrenceEvidence[0].pos;
				if (usedWindowsize < 0) usedWindowsize += 62500;
			}
			if (cursorNode != null) {
				if (usedWindowsize <= windowsize[phfNo][sigNo]) {
					sigMatched[phfNo][sigNo] = true;
					if (!newResult.phf2Credit.containsKey(phfName)){
						newResult.phf2Credit.put(phfName, 0.0);					
					}
					if (accumulatedCredit[phfNo] == 0) 
						accumulatedCredit[phfNo] = singleCredit[phfNo][sigNo];
					else
						accumulatedCredit[phfNo] *= singleCredit[phfNo][sigNo];
					newResult.phf2Credit.put(phfName, accumulatedCredit[phfNo]);
					/**
					 * A phf accumulatedCredit is the product of singleCredit of all matched phf signatures so far.
					 * Credit mechanism: only when the phf accumulatedCredit exceeds the threshold, an alarming will be triggered. 
					 */
					if (!phfMatched[phfNo] && accumulatedCredit[phfNo] >= detectionThreshold[phfNo]) {
						System.out.println(phfName + " on tid:" + tk.tid + " on pid:" + tk.pid + " on pcid:" + tk.pcid + " time: " + firstOccurrenceEvidence[sequenceSignature[phfNo][sigNo].size() - 1].absoluteTime);
						phfMatched[phfNo] = true;
						try {
							BufferedWriter detectionReport = new BufferedWriter(new FileWriter(ActiveMQConsumer.detectionReport.get(tk.pcid), true));
							detectionReport.write(phfName + " on tid:" + tk.tid + " on pid:" + tk.pid + " on pcid:" + tk.pcid + " time: " + firstOccurrenceEvidence[sequenceSignature[phfNo][sigNo].size() - 1].absoluteTime);
							detectionReport.newLine();
							detectionReport.write("from evidence signature: ");
							for (int nowSigNo = 0; nowSigNo < phfSignatureCnt[phfNo]; ++nowSigNo)
							if (sigMatched[phfNo][nowSigNo])
								detectionReport.write(" " + String.valueOf(nowSigNo));
							detectionReport.newLine();
							detectionReport.close();
							if (communication != null) communication.SendReportToSchdular(Integer.valueOf(tk.tid), Integer.valueOf(tk.pid), phf[phfNo]);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					
					/**
					 * Every matched signature will be recorded in detectionDetails along with the change of accumulatedCredit.
					 */
					try {
						BufferedWriter detectionDetails = new BufferedWriter(new FileWriter(ActiveMQConsumer.detectionDetails.get(tk.pcid), true));
						detectionDetails.write(phfName + " sigNo:" + Integer.toString(sigNo) + " on tid:" + tk.tid + " on pid:" + tk.pid + " on pcid:" 
								+ tk.pcid + " time: " + firstOccurrenceEvidence[sequenceSignature[phfNo][sigNo].size() - 1].absoluteTime + " usedWindowSize: " + usedWindowsize + " limitedWindowSize: " + windowsize[phfNo][sigNo] 
								+ " single credit: " + String.valueOf(singleCredit[phfNo][sigNo])+ " present credit: " + String.valueOf(accumulatedCredit[phfNo]));
						detectionDetails.newLine();
						for (int i = 0; i < sequenceSignature[phfNo][sigNo].size(); ++i) {
							WFANode evidenceItr = firstOccurrenceEvidence[i];
							detectionDetails.write(String.valueOf(evidenceItr.originalPos) + " ");
						}
						detectionDetails.newLine();
						detectionDetails.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		} else {
			/**
			 * If it is set-based detection, check all tokens in the signature about whether it has enough number of occurrence in the traceSet.
			 */
			boolean flag = true;
			for (EventRecordM eventRM : setSignature[phfNo][sigNo].keySet()) {
				if (!(traceSet.containsKey(eventRM) && traceSet.get(eventRM) >= setSignature[phfNo][sigNo].get(eventRM))) {
					flag = false;
					break;
				}
			}
			if (flag) {
				sigMatched[phfNo][sigNo] = true;
				if (!newResult.phf2Credit.containsKey(phfName)){
					newResult.phf2Credit.put(phfName, 0.0);	
				}
				if (accumulatedCredit[phfNo] == 0) 
					accumulatedCredit[phfNo] = singleCredit[phfNo][sigNo];
				else
					accumulatedCredit[phfNo] *= singleCredit[phfNo][sigNo];
				newResult.phf2Credit.put(phfName, accumulatedCredit[phfNo]);
				/**
				 * Credit alarming mechanism again.
				 */
				if (!phfMatched[phfNo] && accumulatedCredit[phfNo] >= detectionThreshold[phfNo]) {
					System.out.println(phfName + " on tid:" + tk.tid + " on pid:" + tk.pid + " on pcid:" + tk.pcid + " time: " + traceList.getLast().absoluteTime);
					phfMatched[phfNo] = true;
					try {
						BufferedWriter detectionReport = new BufferedWriter(new FileWriter(ActiveMQConsumer.detectionReport.get(tk.pcid), true));
						detectionReport.write(phfName + " on tid:" + tk.tid + " on pid:" + tk.pid + " on pcid:" + tk.pcid + " time: " + traceList.getLast().absoluteTime);
						detectionReport.newLine();
						detectionReport.write("from evidence signature: ");
						for (int nowSigNo = 0; nowSigNo < phfSignatureCnt[phfNo]; ++nowSigNo)
						if (sigMatched[phfNo][nowSigNo])
							detectionReport.write(" " + String.valueOf(nowSigNo));
						detectionReport.newLine();
						detectionReport.close();
						if (communication != null) communication.SendReportToSchdular(Integer.valueOf(tk.tid), Integer.valueOf(tk.pid), phf[phfNo]);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				try {
					BufferedWriter detectionDetails = new BufferedWriter(new FileWriter(ActiveMQConsumer.detectionDetails.get(tk.pcid), true));
					detectionDetails.write(phfName + " sigNo:" + Integer.toString(sigNo) + " on tid:" + tk.tid + " on pid:" + tk.pid + " on pcid:" 
							+ tk.pcid + " time: " + traceList.getLast().absoluteTime + " single credit: " 
							+ String.valueOf(singleCredit[phfNo][sigNo])+ " present credit: " + String.valueOf(accumulatedCredit[phfNo]));
					detectionDetails.newLine();
					detectionDetails.write(String.valueOf(traceList.getFirst().originalPos) + " " + String.valueOf(traceList.getLast().originalPos));
					detectionDetails.newLine();
					detectionDetails.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * detect(boolean forPhf) is to do the real job to trigger detection.
	 * A "true" forSequence will only check the detection for sequence-based phfs.
	 */
	@Override
	public void detect(boolean forSequence) {
		Result newResult = new Result();
		for (int phfNo = 0 ; phfNo < MaxPhfCnt; ++phfNo) 
		if (isSequence[phfNo] && forSequence) {
			for (int sigNo = 0; sigNo < phfSignatureCnt[phfNo]; ++sigNo) 
			if (!sigMatched[phfNo][sigNo]) {
				testStartAt(root, sigNo, phfNo, newResult);
			}
		} else 
		if (!isSequence[phfNo] && !forSequence){
			for (int sigNo = 0; sigNo < phfSignatureCnt[phfNo]; ++sigNo) 
			if (!sigMatched[phfNo][sigNo]) {
				testStartAt(null, sigNo, phfNo, newResult);
			}
		}
		/**
		 *  publish the results the detector get
		 */
		if (!newResult.phf2Credit.isEmpty())
			publisher.output(newResult);
	}

	@Override
	public void inputPHF(EventRecord_NormalTest record) {
		/**
		 * Update WFA.
		 * Add one or two more nodes in the end of the main chain of WFA.
		 * Remove the first one or two nodes according to max sequence-based windowsize.
		 */
		++detectedSequenceTraceEventCnt;
		if (detectedSequenceTraceEventCnt % 10000 == 0)
			System.out.println("pid" + tk.pid + "_tid" + tk.tid + "_pcid" + tk.pcid + "_SEQ:" + detectedSequenceTraceEventCnt + " Timestamp: " + System.currentTimeMillis());
		
		if (usefulParameter.containsKey(record.eventType)) {
			/**
			 * Add nodes.
			 */
			WFANode[] newlast = new WFANode[uniqueEventCnt + 1];
			for (int j = 0; j <= uniqueEventCnt; ++j) newlast[j] = last[j];
			for (String itrParameter : usefulParameter.get(record.eventType)) 
			if (itrParameter.equals("") || itrParameter.equals(record.parameter)) {
				EventRecordM eventRM = new EventRecordM(record.eventType, itrParameter);
				int nextEventNum = mapping.get(eventRM);
				WFANode newNode = new WFANode(uniqueEventCnt);
				WFA.add(newNode);
				for (int j = 0; j <= uniqueEventCnt; ++j) 
					for (WFANode iterNode = last[j]; iterNode != null && iterNode.succ[nextEventNum] == null; iterNode = iterNode.pred)
						iterNode.succ[nextEventNum] = newNode;
				newNode.pred = last[nextEventNum];
				newNode.pos = record.order;
				newNode.originalPos = record.originalOrder;
				newNode.absoluteTime = record.absoluteTime;
				newlast[nextEventNum] = newNode;
			}
			last = newlast;
			
			/**
			 * Remove the extra eventRecord
			 */
			if (WFA.size() > 0) {
				int presentWindowsize = WFA.getLast().pos - WFA.getFirst().pos;
				if (presentWindowsize < 0) presentWindowsize += 62500;
				while (presentWindowsize > sequenceSignatureMaxWindowsize) {
					for (int i = 0; i < uniqueEventCnt; ++i)
					if (root.succ[i] == WFA.getFirst()) {
						root.succ[i] = WFA.getFirst().succ[i];
						if (WFA.getFirst().succ[i] != null) 
							WFA.getFirst().succ[i].pred = root;
						else
							last[i] = null;
						break;
					}
					WFA.removeFirst();
					presentWindowsize = WFA.getLast().pos - WFA.getFirst().pos;
					if (presentWindowsize < 0) presentWindowsize += 62500;
				}
			}
			
			/**
			 * For WFA is changed, trigger the detection.
			 */
			detect(true);
		}
	}
	
	@Override
	public void inputInit(EventRecord_NormalTest record) {
		/**
		 * Update traceList and traceSet.
		 */
		++detectedSetTraceEventCnt;
		if (detectedSetTraceEventCnt % 10000 == 0)
			System.out.println("pid" + tk.pid + "_tid" + tk.tid + "_pcid" + tk.pcid + "_INIT:" + detectedSetTraceEventCnt + " Timestamp: " + System.currentTimeMillis());
		
		EventRecordM eventRM = new EventRecordM(record.eventType, record.parameter);
		eventRM.absoluteTime = record.absoluteTime;
		eventRM.originalPos = record.originalOrder;
		if (!traceSet.containsKey(eventRM)) 
			traceSet.put(eventRM, 1);
		else
			traceSet.put(eventRM, traceSet.get(eventRM) + 1);
		traceList.add(eventRM);
		 
		if (traceList.size() > setSignatureMaxWindowsize) {
			eventRM = traceList.getFirst();
			traceSet.put(eventRM, traceSet.get(eventRM) - 1);
			traceList.removeFirst();
		}
		
		/**
		 * For traceList and traceSet are changed, trigger the detection.
		 */
		detect(false);
	}

}
