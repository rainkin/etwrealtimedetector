package NU.ETWRealTimeDetector.Match;

import java.util.List;
import java.util.Set;

public class SigResults{
	public Set<String> traces;
	public List<String> sig;
	public int windowsize;
	
	public double coverage;
	public double falsePositiveShare;
	
	

	public SigResults(Set<String> traces, List<String> sig, int windowsize) {
		super();
		this.traces = traces;
		this.sig = sig;
		this.windowsize = windowsize;
	}

	public SigResults() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sig == null) ? 0 : sig.hashCode());
		result = prime * result + ((traces == null) ? 0 : traces.hashCode());
		result = prime * result + windowsize;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SigResults other = (SigResults) obj;
		if (sig == null) {
			if (other.sig != null)
				return false;
		} else if (!sig.equals(other.sig))
			return false;
		if (traces == null) {
			if (other.traces != null)
				return false;
		} else if (!traces.equals(other.traces))
			return false;
		if (windowsize != other.windowsize)
			return false;
		return true;
	}

}
