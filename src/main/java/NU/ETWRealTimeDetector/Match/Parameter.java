package NU.ETWRealTimeDetector.Match;

import NU.ETWRealTimeDetector.EventRecord_NormalTest;

public class Parameter {
	public String para;
	public int pos;
	
	Parameter(String para, int pos) {
		this.para = para;
		this.pos = pos;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((para == null) ? 0 : para.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parameter other = (Parameter) obj;
		if (!para.equals(other.para))
			return false;
		return true;
	}
}
