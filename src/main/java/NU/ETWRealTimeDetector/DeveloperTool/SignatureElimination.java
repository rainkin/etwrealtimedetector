package NU.ETWRealTimeDetector.DeveloperTool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import NU.ETWRealTimeDetector.Match.SigResults;

public class SignatureElimination {
	public final static int MaxSigCnt = 10000;
	public final static int MaxPhfCnt = 5;
	public final static int MaxSeqSigLen = 2000;
	public static final String signatureFolder = "signatures/20170911/";
	public static final String signatureFileExtention = "_orz.txt";
	public static final String signatureOutputFileExtention = "_orz2.txt";
	
	public final static String[] phf = {
			//"audiorecord",
			"keylogger", 
			"remotedesktop",
			"remoteshell",
			"send&execution",
			"urldownload"
	};
	
	public static List<SigResults>[] sigResults = new List[MaxPhfCnt];
	public static List<SigResults>[] newSigResults = new List[MaxPhfCnt];
	public static HashSet<Integer>[] FPsignature = new HashSet[MaxPhfCnt];
	
	public static void main(String[] args) throws IOException {
		for (int phfNo = 0 ; phfNo < MaxPhfCnt; ++phfNo) {
			try {
				sigResults[phfNo] = new Gson().fromJson(new FileReader(signatureFolder + phf[phfNo] + signatureFileExtention), new TypeToken<List<SigResults>>(){}.getType());
			} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
				e.printStackTrace();
			}
			BufferedReader br = new BufferedReader(new FileReader("fpFile/round2/" + phf[phfNo] + ".txt"));
			String data;
			FPsignature[phfNo] = new HashSet<Integer>();
			while ((data = br.readLine()) != null) {
				FPsignature[phfNo].add(Integer.parseInt(data));
			}
			System.out.println(phf[phfNo] + ":" + FPsignature[phfNo].size());
			br.close();
			newSigResults[phfNo]= new ArrayList<SigResults>();
			for (int i = 0; i < sigResults[phfNo].size(); ++i) 
			if (!FPsignature[phfNo].contains(i)) {
				newSigResults[phfNo].add(sigResults[phfNo].get(i));
			}
			File filteredTrace = new File(signatureFolder + phf[phfNo] + signatureOutputFileExtention);
			FileWriter filteredWriter = new FileWriter(filteredTrace);
			filteredWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(newSigResults[phfNo]));
			filteredWriter.close();
		}
	}
}
