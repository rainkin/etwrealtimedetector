package NU.ETWRealTimeDetector.DeveloperTool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import NU.ETWRealTimeDetector.Match.SigResults;

public class TinyModification {
	public final static String[] phf = {
										"audiorecord",
										"keylogger", 
										"remotedesktop",
										"remoteshell",
										"send&execution",
										"urldownload"
										};
	public final static int MaxSigCnt = 10000;
	public final static int MaxPhfCnt = 6;
	public static final String signatureFolder = "signatures/20170911/";
	public static final String signatureFileExtention = "_filtered.txt";
	public static final String signatureOutputFileExtention = "_modified.txt";
	
	public static List<SigResults>[] sigResults = new List[MaxPhfCnt];
	
	public static void main(String[] args) throws IOException {
		for (int phfNo = 0 ; phfNo < MaxPhfCnt; ++phfNo) {
			try {
				sigResults[phfNo] = new Gson().fromJson(new FileReader(signatureFolder + phf[phfNo] + signatureFileExtention), new TypeToken<List<SigResults>>(){}.getType());
			} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
				e.printStackTrace();
			}
			int sigCnt = sigResults[phfNo].size();
			for (int i = 0; i < sigCnt; ++i) {
				for (int j = 0; j < sigResults[phfNo].get(i).sig.size();) {
					String sigUnit = sigResults[phfNo].get(i).sig.get(j);
					String[] splitResults = sigUnit.split(" @ ");
					if ((splitResults[0].equals("TCP_Send") && splitResults[1].equals("None:None")) ||
						(splitResults[0].equals("RegistrySetInformation") && splitResults[1].equals("KeyName:"))) {
						sigResults[phfNo].get(i).sig.remove(j);
					} else {
						String[] splitResults2 = splitResults[1].split(":");
						if (splitResults2.length >= 2) {
							if (splitResults2[1].equals("XXX.dc")) {
								sigResults[phfNo].get(i).sig.set(j, splitResults[0] + " @ @DataFile");
							} else {
								sigResults[phfNo].get(i).sig.set(j, splitResults[0] + " @ " + splitResults2[1]);
							}
						} else {
							sigResults[phfNo].get(i).sig.set(j, splitResults[0] + " @ ");
						}
						++j;
					}
				}
			}
			File filteredTrace = new File(signatureFolder + phf[phfNo] + signatureOutputFileExtention);
			FileWriter filteredWriter = new FileWriter(filteredTrace);
			filteredWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(sigResults[phfNo]));
			filteredWriter.close();
		}
	}
}
