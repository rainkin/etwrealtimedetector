package NU.ETWRealTimeDetector.DeveloperTool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import NU.ETWRealTimeDetector.Match.SigResults;

public class TransferSignatureFromETWMatchToRealDetector {
	
	public static String[] phfs = { "keylogger", "send&execution", "urldownload", "remotedesktop", "remoteshell" };
	public static String signatureFolder = "signatures/20171108/";
	public static String inputExtention = "_localResults_filtered.txt";
	public static String outputExtention = "_test.txt";
	
	public static void main(String[] args) throws JsonIOException, JsonSyntaxException, IOException {
		List<SigResults> sigResults;
		for (String phf : phfs) {
			File file = new File(signatureFolder + phf + inputExtention);
			System.out.println(file.getName());
			sigResults = new Gson().fromJson(new FileReader(file), new TypeToken<List<SigResults>>() {
			}.getType());
			for (SigResults nowSig : sigResults) {
				for (int j = 0; j < nowSig.sig.size(); ++j) {
					String[] splitResults = nowSig.sig.get(j).split(" @ ");
					String[] splitResults2 = splitResults[1].split(":");
					if (splitResults2.length > 1) {
						nowSig.sig.set(j, splitResults[0] + " @ " + splitResults2[1]);
					} else {
						nowSig.sig.set(j, splitResults[0] + " @ ");
					}
				}
			}
			File filteredTrace = new File(signatureFolder + phf + outputExtention);
			FileWriter filteredWriter = new FileWriter(filteredTrace);
			filteredWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(sigResults));
			filteredWriter.close();
		}
	}
}
