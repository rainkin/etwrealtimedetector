package NU.ETWRealTimeDetector.Main;

import java.io.IOException;

import NU.ETWRealTimeDetector.Manager;
import NU.ETWRealTimeDetector.Input.ActiveMQSingleThreadConsumer;
import NU.ETWRealTimeDetector.Input.Consumer;
import NU.ETWRealTimeDetector.Match.Detector;
import NU.ETWRealTimeDetector.Match.TraceKey;
import NU.ETWRealTimeDetector.Match.WFADetector;
import NU.ETWRealTimeDetector.Output.ActiveMQPublisher;
import NU.ETWRealTimeDetector.Output.Publisher;

/**
 * This is the main class for a detection system running one client machine.
 * Scheduler will create an object of it to start the detection job on every single machine.
 * Parameters serve as the ActiveMQ tunnel information passed to ActiveMQSingleThreadConsumer object created here.
 * @author ander
 * 
 */
public class ActiveMQSingleThreadConsumerMain {
	
	protected String user;
	protected String password;
	protected String url;
	protected String destination;
	protected boolean needDump = false;
	protected String clientName;
	
	public ActiveMQSingleThreadConsumerMain(String user, String password, String url,  String destination, boolean needDump, String name) throws IOException, InterruptedException {
		this.user = user;
		this.password = password;
		// host = "localhost";
		// host = "10.214.148.122";
		// url ="tcp://host:61616"
		this.url = url;
		this.destination = destination;
		this.needDump = needDump;
		this.clientName = name;
		start();
	}
	
	public void start() throws IOException, InterruptedException {
		Manager manager = new Manager();
		Consumer consumer = new ActiveMQSingleThreadConsumer(clientName, user, password, url, destination, needDump);
		Detector detector = new WFADetector(new TraceKey());
		Publisher publisher = new ActiveMQPublisher();
		//Publisher publisher = new StdPublisher();
		manager.setConsumer(consumer);
		manager.setDetector(detector);
		manager.setPublisher(publisher);
		manager.start();
		Thread t = new Thread();
	}

}
