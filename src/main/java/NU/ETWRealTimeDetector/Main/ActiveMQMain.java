package NU.ETWRealTimeDetector.Main;

import java.io.IOException;

import NU.ETWRealTimeDetector.Manager;
import NU.ETWRealTimeDetector.Input.ActiveMQConsumer;
import NU.ETWRealTimeDetector.Input.Consumer;
import NU.ETWRealTimeDetector.Match.Detector;
import NU.ETWRealTimeDetector.Match.TraceKey;
import NU.ETWRealTimeDetector.Match.WFADetector;
import NU.ETWRealTimeDetector.Output.ActiveMQPublisher;
import NU.ETWRealTimeDetector.Output.Publisher;

/**
 * This is the Main class for invoking ActiveMQConsumer.
 * @author ander
 *
 */
public class ActiveMQMain {

	public static void main(String[] args) throws IOException {
				
		Manager manager = new Manager();
		Consumer consumer = new ActiveMQConsumer();
		Detector detector = new WFADetector(new TraceKey());
		Publisher publisher = new ActiveMQPublisher();
		//Publisher publisher = new StdPublisher();
		manager.setConsumer(consumer);
		manager.setDetector(detector);
		manager.setPublisher(publisher);
		
		manager.start();
	}

}
