package NU.ETWRealTimeDetector.Main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import NU.ETWRealTimeDetector.Manager;
import NU.ETWRealTimeDetector.Input.ActiveMQConsumer;
import NU.ETWRealTimeDetector.Input.ChunlinFileConsumer;
import NU.ETWRealTimeDetector.Input.Consumer;
import NU.ETWRealTimeDetector.Input.DumpFileConsumer;
import NU.ETWRealTimeDetector.Match.Detector;
import NU.ETWRealTimeDetector.Match.TraceKey;
import NU.ETWRealTimeDetector.Match.WFADetector;
import NU.ETWRealTimeDetector.Output.ActiveMQPublisher;
import NU.ETWRealTimeDetector.Output.Publisher;
import NU.ETWRealTimeDetector.Output.StdPublisher;

/**
 * This is the Main class for invoking DumpFileConsumer.
 * testFiles refers to the trace files the consumer will try to consume.
 * traceExtenstion refers to the suffix of the trace files.
 * @author ander
 *
 */
public class DumpFileMain {

	public static void main(String[] args) throws IOException {
				
		Manager manager = new Manager();
		
		List<String> testFiles = new ArrayList<String>();
		testFiles.add("testTraces/ZYTrace/tid6952_pid8132_pcid82.txt");
		Consumer consumer = new DumpFileConsumer(testFiles);
		Detector detector = new WFADetector(new TraceKey());
		Publisher publisher = new StdPublisher();
		manager.setConsumer(consumer);
		manager.setDetector(detector);
		manager.setPublisher(publisher);
		
		manager.start();
	}

}
