package NU.ETWRealTimeDetector.Main;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import NU.ETWRealTimeDetector.Manager;
import NU.ETWRealTimeDetector.Input.ActiveMQConsumer;
import NU.ETWRealTimeDetector.Input.ChunlinFileConsumer;
import NU.ETWRealTimeDetector.Input.Consumer;
import NU.ETWRealTimeDetector.Input.DumpFileConsumer;
import NU.ETWRealTimeDetector.Input.ETWTraceFileConsumer;
import NU.ETWRealTimeDetector.Match.Detector;
import NU.ETWRealTimeDetector.Match.TraceKey;
import NU.ETWRealTimeDetector.Match.WFADetector;
import NU.ETWRealTimeDetector.Output.ActiveMQPublisher;
import NU.ETWRealTimeDetector.Output.Publisher;
import NU.ETWRealTimeDetector.Output.StdPublisher;

/**
 * This is the Main class for invoking ETWTraceFileConsumer.
 * traceDirPath refers to the directory the program will find the trace files.
 * traceExtenstion refers to the suffix of the trace files.
 * @author ander
 *
 */
public class ETWTraceFileMain {

	public static String traceExtension = ".output";
	public static String traceDirPath = "testTraces/OfflineTest/";

	public static void main(String[] args) throws IOException {

		Manager manager = new Manager();
		
		File dir = new File(traceDirPath);
		File[] files = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(traceExtension);
			}
		});
		
		List<String> testFiles = new ArrayList<String>();
		//testFiles.add("testTraces/FP4_Shadowsocks.exe_PID2104_TID6204.output.tracePreprocessed");
		for (File file : files)
			testFiles.add(file.getPath());
		
		Consumer consumer = new ETWTraceFileConsumer(testFiles);
		Detector detector = new WFADetector(new TraceKey());
		Publisher publisher = new StdPublisher();
		manager.setConsumer(consumer);
		manager.setDetector(detector);
		manager.setPublisher(publisher);

		manager.start();
	}

}
