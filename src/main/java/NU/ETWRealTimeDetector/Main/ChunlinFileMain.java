package NU.ETWRealTimeDetector.Main;

import java.util.ArrayList;
import java.util.List;

import NU.ETWRealTimeDetector.Manager;
import NU.ETWRealTimeDetector.Input.ChunlinFileConsumer;
import NU.ETWRealTimeDetector.Input.Consumer;
import NU.ETWRealTimeDetector.Match.Detector;
import NU.ETWRealTimeDetector.Match.TraceKey;
import NU.ETWRealTimeDetector.Match.WFADetector;
import NU.ETWRealTimeDetector.Output.ActiveMQPublisher;
import NU.ETWRealTimeDetector.Output.Publisher;
import NU.ETWRealTimeDetector.Output.StdPublisher;

public class ChunlinFileMain {

	public static void main(String[] args) {
				
		Manager manager = new Manager();
		
		List<String> testFiles = new ArrayList<String>();
		testFiles.add("testTraces/traceofETW_1min/traceofETW_1min.txt");
		Consumer consumer = new ChunlinFileConsumer(testFiles);
		Detector detector = new WFADetector(new TraceKey());
		Publisher publisher = new StdPublisher();
		manager.setConsumer(consumer);
		manager.setDetector(detector);
		manager.setPublisher(publisher);
		
		manager.start();
	}

}
