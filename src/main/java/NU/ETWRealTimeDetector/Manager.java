package NU.ETWRealTimeDetector;

import NU.ETWRealTimeDetector.Input.Consumer;
import NU.ETWRealTimeDetector.Match.Detector;
import NU.ETWRealTimeDetector.Output.Publisher;

public class Manager {
	protected Consumer consumer;
	protected Detector detector;
	protected Publisher publisher;
	
	public Consumer getConsumer() {
		return consumer;
	}
	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}
	public Detector getDetector() {
		return detector;
	}
	public void setDetector(Detector detector) {
		this.detector = detector;
	}
	public Publisher getPublisher() {
		return publisher;
	}
	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
	
	public void start() {
		consumer.setDetector(detector);
		detector.setPublisher(publisher);
		consumer.start();		
	}
	
	
}
