package NU.ETWRealTimeDetector.GUI;

//Imports are listed in full to show what's being used
//could just import javax.swing.* and java.awt.* etc..
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import NU.ETWRealTimeDetector.Match.Detector;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.awt.event.ActionEvent;

public class GUIDemo {

	protected String EtwProviderPath = "ETW.exe";
	protected String EtwPreprocessorPath = "DIA_findSymbolByVA.exe";
	protected String RunnableJarPath = "ETWRealTimeDectector-0.0.1-SNAPSHOT-jar-with-dependencies.jar";

	// Note: Typically the main method will be in a
	// separate class. As this is a simple one class
	// example it's all in the one class.
	public static void main(String[] args) {

		new GUIDemo();
	}

	public GUIDemo() {
		JFrame guiFrame = new JFrame();

		// make sure the program exits when the frame closes
		guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		guiFrame.setTitle("ETW Real-time Detector");
		guiFrame.setSize(500, 500);

		// This will center the JFrame in the middle of the screen
		guiFrame.setLocationRelativeTo(null);

		// pannel

		// button
		JButton startETWButton = new JButton("Start Tracing");
		startETWButton.setPreferredSize(new Dimension(200, 50));
		startETWButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				execExe(EtwProviderPath, "ETW/", true);
				execExe(EtwPreprocessorPath, "ETW/", true);
				
			}
		});
		
		JButton startDetectingButton = new JButton("Start Detecting");
		startDetectingButton.setPreferredSize(new Dimension(200, 50));
		startDetectingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {				
				execJar(RunnableJarPath, "NU.ETWRealTimeDetector.Main.ActiveMQMain", ".", true);
			}
		});
		
		JButton openDetectionResultButton = new JButton("Show Detection Results");
		openDetectionResultButton.setPreferredSize(new Dimension(50, 50));
		openDetectionResultButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {				
				execJar(RunnableJarPath, "NU.ETWRealTimeDetector.GUI.ResultListener", ".", false);
			}
		});

		guiFrame.add(startETWButton,BorderLayout.NORTH);
		guiFrame.add(startDetectingButton,BorderLayout.CENTER);
		guiFrame.add(openDetectionResultButton,BorderLayout.SOUTH);

		// make sure the JFrame is visible
		guiFrame.setVisible(true);
	}
	
	/*
	 * exec **.exe from the workDir
	 */
	private void execExe(String cmd, String workDir, boolean isMin){
		ProcessBuilder pb = null;
		if (isMin){
			pb = new ProcessBuilder("cmd", "/c", "start", "/min", cmd);
		}
		else {
			pb = new ProcessBuilder("cmd", "/c", "start", cmd);
		}
		pb.redirectErrorStream(true);
	

		try {
			pb.directory(new File(workDir));
			Process p = pb.start();
			
			// log error info
			BufferedReader errorReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			StringBuilder errorBuilder = new StringBuilder();
			String line = null;
			while ((line = errorReader.readLine()) != null) {
				errorBuilder.append(line);
				errorBuilder.append(System.getProperty("line.separator"));
			}
			String error = errorBuilder.toString();
			FileWriter logWriter = new FileWriter("log.txt", true);
			logWriter.write("ERROR: " + error + "\n");
			System.out.println("ERROR: " + error);
			
			// log output info
			BufferedReader outputReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			StringBuilder outputBuilder = new StringBuilder();
			line = null;
			while ((line = errorReader.readLine()) != null) {
				errorBuilder.append(line);
				errorBuilder.append(System.getProperty("line.separator"));
			}
			String output = errorBuilder.toString();
			logWriter.write("OUTPUT: " + output + "\n\n");
			System.out.println("OUTPUT: " + output);
			logWriter.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * exec **.jar from the workDir
	 */
	private void execJar(String jarName, String runnableClassName, String workDir, boolean isMin){
		ProcessBuilder pb = null;
		if (isMin){
			pb = new ProcessBuilder("cmd", "/c", "start", "/min", "java", "-cp", jarName, runnableClassName);
		}
		else {
			pb = new ProcessBuilder("cmd", "/c", "start", "java", "-cp", jarName, runnableClassName);
		}
		pb.redirectErrorStream(true);
	

		try {
			pb.directory(new File(workDir));
			Process p = pb.start();
			
			// log error info
			BufferedReader errorReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			StringBuilder errorBuilder = new StringBuilder();
			String line = null;
			while ((line = errorReader.readLine()) != null) {
				errorBuilder.append(line);
				errorBuilder.append(System.getProperty("line.separator"));
			}
			String error = errorBuilder.toString();
			System.out.println("ERROR: " + error);
			
			// log output info
			BufferedReader outputReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			StringBuilder outputBuilder = new StringBuilder();
			line = null;
			while ((line = errorReader.readLine()) != null) {
				errorBuilder.append(line);
				errorBuilder.append(System.getProperty("line.separator"));
			}
			String output = errorBuilder.toString();
			System.out.println("OUTPUT: " + output);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}