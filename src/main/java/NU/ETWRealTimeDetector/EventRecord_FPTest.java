package NU.ETWRealTimeDetector;

import java.util.Map;

public class EventRecord_FPTest {
	public String processID;
	public String processName;
	public String threadID;
	public String eventName;
	public Map<String, String> arguments;
	public Integer PCid;
	
	public EventRecord_FPTest() {
		
	}
	
	public EventRecord_FPTest(String processID, String threadID, String eventName, Map<String, String> arguments, int PCid) {
		this.processID = processID;
		this.threadID = threadID;
		this.eventName = eventName;
		this.arguments = arguments;
		this.PCid = PCid;
	}
}
