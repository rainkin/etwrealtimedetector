package NU.ETWRealTimeDetector.Input;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import NU.ETWRealTimeDetector.EventRecord_NormalTest;
import NU.ETWRealTimeDetector.Utils;
import NU.ETWRealTimeDetector.Match.Detector;
import NU.ETWRealTimeDetector.Match.SigResults;


public class ChunlinFileConsumer extends Consumer{
	protected List<String> files;
	protected int idCount = 0;
	protected int MaxCacheNumber = 500;
	protected int currentCacheNumber = 0;
	protected int maxBlockSize = 500;
	
	public ChunlinFileConsumer(List<String> files) {
		this.files = files;
	}

	@Override
	public void start() {
		files.forEach(f->{
			try {
				consumerFromFile(f);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}
	
	private EventRecord_NormalTest convertETWJson2Object(String json){
		return new Gson().fromJson(json, EventRecord_NormalTest.class);
	}
	
	private void consumerFromFile(String fileName) throws IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		System.out.println("START CONSUMERING :" + fileName);
		List<EventRecord_NormalTest> syscallrecords = new ArrayList<EventRecord_NormalTest>();
		String data;
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		while ((data = br.readLine()) != null) {
			EventRecord_NormalTest record = convertETWJson2Object(data);
			syscallrecords.add(record);
		}
		System.out.println("Start Timestamp: " + System.currentTimeMillis());
		Detector newDetector = this.detector.getClass().getConstructor(Integer.class).newInstance(0);
		newDetector.setPublisher(this.detector.getPublisher());
		for (EventRecord_NormalTest inputRecord : syscallrecords) {
			newDetector.inputPHF(inputRecord);
		}
		System.out.println("End Timestamp: " + System.currentTimeMillis());
	}

}
