package NU.ETWRealTimeDetector.Input;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import NU.ETWRealTimeDetector.EventRecord_NormalTest;
import NU.ETWRealTimeDetector.EventRecord;
import NU.ETWRealTimeDetector.EventRecordG;
import NU.ETWRealTimeDetector.Utils;
import NU.ETWRealTimeDetector.Match.Detector;
import NU.ETWRealTimeDetector.Match.SigResults;
import NU.ETWRealTimeDetector.Match.TraceKey;


public class ETWTraceFileConsumer extends Consumer{
	
	public static int numberOfBlocksToKeep = 2;
	public static int minLengthOfConsecutiveBlock = 1;
	public static int maxLengthOfConsecutiveBlock = 500;
	
	protected List<String> files;
	protected int idCount = 0;
	protected int MaxCacheNumber = 10000;
	protected int currentCacheNumber = 0;
	protected int maxBlockSize = 500;
	Map<TraceKey, Integer> testCount = new HashMap<TraceKey, Integer>();
	
	boolean preprocessed = false;
	
	public ETWTraceFileConsumer(List<String> files) {
		this.files = files;
	}

	@Override
	public void start() {
		files.forEach(f->{
			try {
				consumerFromFile(f);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}
	
	private EventRecordG convertETWJson2Object(String json){
		return new Gson().fromJson(json, EventRecordG.class);
	}
	
	private EventRecord convertETWJson2Object2(String json){
		return new Gson().fromJson(json, EventRecord.class);
	}
	
	private void consumerFromFile(String filePath) throws IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		/**
		 * Get tid and pid from filePath 
		 * And set up the .onlinePreprocessed dump file.
		 * Theoretically .onlinePreprocessed trace files should be exactly equal to .tracePreprocessed trace files
		 */
		String finalFileName = filePath.split("\\\\")[filePath.split("\\\\").length-1];
		int index = finalFileName.indexOf("PID");
		finalFileName = finalFileName.substring(index).split("\\.")[0];
		String[] splitResults = finalFileName.split("_");
		String pid = splitResults[0].split("PID")[1];
		String tid = splitResults[1].split("TID")[1];
		//System.out.println("Consuming PID" + pid + "_TID" + tid);
		BufferedWriter bw = new BufferedWriter(new FileWriter(filePath + ".onlinePreprocessed"));
		
		/**
		 * Read in trace file and convert it into EventRecord_NormalTest traces
		 */
		System.out.println("START CONSUMERING :" + filePath);
		List<EventRecord_NormalTest> original_eventRecords = new ArrayList<EventRecord_NormalTest>();
		String data;
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		//int line = 1;
		while ((data = br.readLine()) != null) {
			//System.out.println(line);
			EventRecord_NormalTest record = null;
			if (preprocessed) {
				EventRecordG tmpRecord = convertETWJson2Object(data);
				record = new EventRecord_NormalTest(-1, "-1", tmpRecord.name, "-1", "-1", tid, 
				   pid, tmpRecord.parameter, -1, "-1");
			} else {
				EventRecord tmpRecord = convertETWJson2Object2(data);
				record = Utils.selectArgFields(tmpRecord, tid, pid);
			}
			if (record == null) continue;
			original_eventRecords.add(record);
		}
		br.close();
		
		/**
		 * Start detection
		 * Imitate the detecting process in real-time mode(cache records -> preprocessing -> detect)
		 */
		System.out.println("Start Timestamp: " + System.currentTimeMillis());
		/**
		 * Set up details and report files.
		 * Get traceKey and corresponding detector object.
		 */
		ActiveMQConsumer.detectionDetails.put("-1", "detectionDetails/details_pcid-1.txt");
		ActiveMQConsumer.detectionReport.put("-1","detectionReport/report_pcid-1.txt");
		/**
		 * By default, pcid for offline mode is -1.
		 */
		TraceKey tk = new TraceKey(tid, pid, "-1");
		Detector newDetector = this.detector.getClass().getConstructor(TraceKey.class).newInstance(tk);
		newDetector.setPublisher(this.detector.getPublisher());
		
		List<EventRecord_NormalTest> cachedOriginalTrace = new ArrayList<EventRecord_NormalTest>();
		testCount.put(tk, 0);
		int count = 0;
		/**
		 * Imitate the real-time detection: cache data record one by one
		 */
		for (int i = 0; i < original_eventRecords.size(); ++i) {
			EventRecord_NormalTest inputRecord = original_eventRecords.get(i);
			cachedOriginalTrace.add(inputRecord);
			++count;
			//System.out.println(count);
			if (count % ActiveMQConsumer.MaxCacheNumber == 0 || i == original_eventRecords.size() - 1) {
				//System.out.println(ActiveMQConsumer.MaxCacheNumber);
				System.out.println("Consumed " + count);
				/**
				 * Seperate phf detection and initialization detection for they have different pre-processing
				 */
				List<EventRecord_NormalTest> phfTrace = new ArrayList<EventRecord_NormalTest>();
				List<EventRecord_NormalTest> initTrace = new ArrayList<EventRecord_NormalTest>();
				/**
				 * Offline mode adopts two versions of offline traces. The first one is pure original traces(.output). The second one is the pre-processed traces(.output.tracePreprocessed).
				 * For the second one we directly use the read-in traces. 
				 */
				if (!preprocessed) {
					phfTrace = Utils.phfTracePreProcess(cachedOriginalTrace, minLengthOfConsecutiveBlock, maxLengthOfConsecutiveBlock, numberOfBlocksToKeep, false);
					for (EventRecord_NormalTest er : phfTrace) {
						EventRecordG tmpRecord = new EventRecordG(er.eventType, er.parameter);
						String output = new GsonBuilder().create().toJson(tmpRecord);
			        	bw.write(output + "\n");
			        	bw.flush();
					}
					initTrace = Utils.initTracePreProcess(cachedOriginalTrace, false);
				} else {
					phfTrace = cachedOriginalTrace;
					initTrace = cachedOriginalTrace;
				}
				/**
				 * Go along cached trace and detect()
				 */
				for (EventRecord_NormalTest realInputRecord : phfTrace) {
					realInputRecord.order = testCount.get(tk);
					realInputRecord.originalOrder = realInputRecord.order;
					testCount.put(tk, (testCount.get(tk) + 1) % 62500);
					newDetector.inputPHF(realInputRecord);
				}
				for (EventRecord_NormalTest realInputRecord : initTrace) {
					newDetector.inputInit(realInputRecord);
				}
				cachedOriginalTrace.clear();
			}
		}
		System.out.println("End Timestamp: " + System.currentTimeMillis());
		bw.close();
	}

}
