/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package NU.ETWRealTimeDetector.Input;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import NU.ETWRealTimeDetector.EventRecord_NormalTest;
import NU.ETWRealTimeDetector.EventRecord_FPTest;
import NU.ETWRealTimeDetector.Utils;
import NU.ETWRealTimeDetector.Match.Detector;
import NU.ETWRealTimeDetector.Match.TraceKey;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.jms.*;
import javax.print.attribute.standard.PrinterMessageFromOperator;

public class ActiveMQConsumer extends Consumer {
	
	public static int numberOfBlocksToKeep = 2;
	public static int minLengthOfConsecutiveBlock = 1;
	public static int maxLengthOfConsecutiveBlock = 500;

	protected String user;
	protected String password;
	protected String host;
	protected int port;
	protected String destination;

	public static int MaxCacheNumber = 10000;
	protected int maxBlockSize = 500;
	protected boolean needDump = false;
	protected boolean fullTraceMode = true;
	protected boolean onlyDumpNeeded = true;
	
	/**
	 * All data will be used for detection related to one single thread.
	 */
	protected Map<TraceKey, List<EventRecord_NormalTest>> traceKey2records = new HashMap<TraceKey, List<EventRecord_NormalTest>>();
	protected Map<TraceKey, Detector> traceKey2detector = new HashMap<TraceKey, Detector>();
	protected Map<TraceKey, Integer> currentReceivedNumber = new HashMap<TraceKey, Integer>();
	protected Map<TraceKey, Integer> currentRoundNumber = new HashMap<TraceKey, Integer>();
	protected Map<TraceKey, Integer> testCount = new HashMap<TraceKey, Integer>();
	Map<TraceKey, BufferedWriter> dumpFile = new HashMap<TraceKey, BufferedWriter>();
	
	Map<Integer, String> opcode_eventtype_map = new HashMap<Integer, String>();
	
	public static Map<String, String> detectionDetails = new HashMap<String, String>();
	public static Map<String, String> detectionReport = new HashMap<String, String>();

	/**
	 * The Consumer class for experimental online detector. NOT integrated with scheduler authored by Liheng but directly integrated with Chunlin's collector 
	 * The constructor below is to set up several connection information needs from config.txt.
	 * They define a specific ActiveMQ data tunnel used by the consumer. 
	 * @throws IOException
	 */
	public ActiveMQConsumer() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("config.txt"));
		user = br.readLine();
		password = br.readLine();
		// host = "localhost";
		// host = "10.214.148.122";
		host = br.readLine();
		port = Integer.parseInt(br.readLine());
		destination = br.readLine();
		int estimatedPCCnt = Integer.parseInt(br.readLine());
		String dumped = br.readLine();
		if (dumped.equals("yes")) needDump = true;
		br.close();
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	/**
	 * json data convertion tools
	 * @param json
	 * @return
	 */
	
	private static List<EventRecord_FPTest> convertETWJson2ObjectList(String json) {
		return new Gson().fromJson(json, new TypeToken<List<EventRecord_FPTest>>(){}.getType());
	}

//	@SuppressWarnings("unused")
//	@Override
	/**
	 * Old start() function
	 * Data messages accepted are consist of a fixed number(10001 for full-kernel mode & 10000 for partial-kernel mode) of data record from all threads, all processes and all client hosts.
	 * Some logics are also not matched to present collector design(It needs cacheRecord() to be in charge of triggering detect(). detect() is triggered when it gets 10000 pre-processed records). 
	 * Basically of no use if no modification is done to match the new collector design.
	 */
	public void startOld() {
		opcode_eventtype_map.put(33, "ALPC_SEN");
		opcode_eventtype_map.put(32, "FileIoCreate");
		opcode_eventtype_map.put(64, "FileIoCreate");
		opcode_eventtype_map.put(74, "FileIoQueryInfo");
		opcode_eventtype_map.put(67, "FileIoRead");
		opcode_eventtype_map.put(68, "FileIoWrite");
		opcode_eventtype_map.put(3, "ImageDCStart");
		opcode_eventtype_map.put(22, "RegistryKCBCreate");
		opcode_eventtype_map.put(11, "RegistryOpen");
		opcode_eventtype_map.put(13, "RegistryQuery");
		opcode_eventtype_map.put(16, "RegistryQueryValue");
		opcode_eventtype_map.put(20, "RegistrySetInformation");
		opcode_eventtype_map.put(51, "SystemCall");
		opcode_eventtype_map.put(110, "ImageLoad");
		opcode_eventtype_map.put(10, "RegistryCreate");
		opcode_eventtype_map.put(34, "ALPC_REC");
		opcode_eventtype_map.put(134, "DiskIoDrvMjFnCal");
		opcode_eventtype_map.put(2, "ThreadEnd");
		opcode_eventtype_map.put(21, "ProcessEnd");
		

		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://" + host + ":" + port);

		Connection connection;
		try {
			connection = factory.createConnection(user, password);
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination dest = session.createTopic(destination);

			
			BufferedWriter dataLogFile = new BufferedWriter(new FileWriter("log/data.log", false));			
			BufferedWriter processThreadLogFile = new BufferedWriter(new FileWriter("log/process_thread.log", false));
			
			
			MessageConsumer consumer = session.createConsumer(dest);
			long start = System.currentTimeMillis();
			long count = 0;
			System.out.println("Waiting for messages...");
			
			while (true) {
				Message msg = null;
				try {
					msg = consumer.receive();
				} catch (Exception e) {
					System.out.println("Consumer reception problem." + e);
				}
				if (msg instanceof BytesMessage) {
					byte[] byteMessage = new byte[90100];
					((BytesMessage) msg).readBytes(byteMessage);
					Date now = new Date();
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					String dateStr = dateFormat.format(now);
					for (int i = 0; i < 10000; i++) {
						int countNumber = getUnsignedByte(byteMessage[i * 9 + 1]) - 1
								+ (getUnsignedByte(byteMessage[i * 9 + 2]) - 1) * 255;
						int pid = getUnsignedByte(byteMessage[i * 9 + 3]) - 1
								+ (getUnsignedByte(byteMessage[i * 9 + 4]) - 1) * 255;
						int threadid = getUnsignedByte(byteMessage[i * 9 + 5]) - 1
								+ (getUnsignedByte(byteMessage[i * 9 + 6]) - 1) * 255;
						int parameterNo = getUnsignedByte(byteMessage[i * 9 + 7]) - 1
								+ (getUnsignedByte(byteMessage[i * 9 + 8]) - 1) * 255;
						int pcid = getUnsignedByte(byteMessage[0]);

						String parameter = decodeList[parameterNo];
						int opcode = getUnsignedByte(byteMessage[i * 9 + 9]) - 1;
						String eventType = "";
						if (opcode_eventtype_map.containsKey(opcode))
							eventType = opcode_eventtype_map.get(opcode);
						else
							System.out.println(opcode);
						if(opcode == 2){
//							System.out.println(eventType + " " + threadid);							
							String currentTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
							processThreadLogFile.write(eventType + "@" + threadid + "@" + pcid + "@" + currentTime);
							processThreadLogFile.newLine();
							processThreadLogFile.flush();
						}
						else if(opcode == 21){
//							System.out.println(eventType + " " + pid);
							String currentTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
							processThreadLogFile.write(eventType + "@" + pid + "@" + pcid + "@" + currentTime);
							processThreadLogFile.newLine();
							processThreadLogFile.flush();
						}
						if (eventType.equals("ImageDCStart")) eventType = "ImageLoad";
						
						EventRecord_NormalTest record = new EventRecord_NormalTest();
						record.pid = Integer.toString(pid);
						record.absoluteTime = dateStr;
						record.tid = Integer.toString(threadid);
						record.eventType = eventType;
						record.parameter = parameter;
						record.originalOrder = countNumber;
						record.order = -1;
						record.pcid = Integer.toString(pcid);
						TraceKey tk = new TraceKey(record.tid, record.pid, record.pcid);
						++count;
						
						// Elminate out-of-date thread.
						if (record.eventType.equals("ThreadEnd")) {
							if (traceKey2records.get(tk) != null) detect(tk);
							traceKey2records.remove(tk);
							traceKey2detector.remove(tk);
							if (dumpFile.get(tk) != null) dumpFile.get(tk).close();
							dumpFile.remove(tk);
							testCount.remove(tk);
							continue;
						}
						
						// Cache new record and dump trace file
						if (!detectionDetails.containsKey(record.pcid)) {
							detectionDetails.put(record.pcid, "detectionDetails/details_pcid" + record.pcid + ".txt");
							detectionReport.put(record.pcid, "detectionReport/report_pcid" + record.pcid + ".txt");
						}
						if (!dumpFile.containsKey(tk)) {
							String newFile = "dumpFile/tid" + record.tid + "_pid" + record.pid + "_pcid"
									+ record.pcid + ".txt";
							dumpFile.put(tk, new BufferedWriter(new FileWriter(newFile)));
						}
						cacheRecord(record);
						
						if (needDump) {
							BufferedWriter fw = dumpFile.get(tk);
							fw.write(record.eventType + " @ " + record.parameter);
							fw.newLine();
							fw.flush();
						}
					}
					
//					for (TraceKey tk : traceKey2records.keySet()) 
//					if (new Date().getTime() - lastDetectionTime.get(tk) > 120000){
//						lastDetectionTime.put(tk, new Date().getTime());
//						System.out.println("Activated on " + lastDetectionTime.get(tk));
//						detect(tk);
//					}
				} else if (msg instanceof TextMessage && fullTraceMode) {
					HashSet<TraceKey> occured = new HashSet<TraceKey>();
					String body = "";
					try {
						body = ((TextMessage) msg).getText();
					} catch (Exception e) {
						System.out.println(msg);
						continue;
					}
					//System.out.println(body);
					List<EventRecord_FPTest> tmpRecords = null;
					try {
						tmpRecords = convertETWJson2ObjectList(body);
					} catch (Exception e) {
						BufferedWriter bw = new BufferedWriter(new FileWriter("err.txt"));
						bw.write(body);
						bw.close();
						System.out.println("Oh shit!");
						System.exit(0);
					}
					//System.out.println(tmpRecords.size());
					
					for (int i = 0; i < 10001; ++i) {
						EventRecord_NormalTest record = new EventRecord_NormalTest();
						record.pid = tmpRecords.get(i).processID;
						record.tid = tmpRecords.get(i).threadID;
						if (tmpRecords.get(i).eventName.equals("PerfInfoSysClEnter")) {
							record.eventType = "SystemCall";
						} else if (tmpRecords.get(i).eventName.equals("ALPCALPC-Send-Message"))
							record.eventType = "ALPC_SEN";
						else if (tmpRecords.get(i).eventName.equals("ALPCALPC-Receive-Message"))
							record.eventType = "ALPC_REC";
						else
							record.eventType = tmpRecords.get(i).eventName;
						record.parameter = "";
						boolean flag = false;
						for (String parameterName : tmpRecords.get(i).arguments.keySet())
							if (!flag) {
								record.parameter = record.parameter + tmpRecords.get(i).arguments.get(parameterName);
								flag = true;
							} else
								record.parameter = record.parameter + " @ " + tmpRecords.get(i).arguments.get(parameterName);

						
						record.pcid = tmpRecords.get(i).PCid.toString();
						TraceKey tk = new TraceKey(record.tid, record.pid, record.pcid);
						record.order = -1;
						count++;
						
						// Elminate out-of-date thread.
						if (record.eventType.equals("ThreadEnd")) {
							if (traceKey2records.get(tk) != null) detect(tk);
							traceKey2records.remove(tk);
							traceKey2detector.remove(tk);
							dumpFile.remove(tk);
							testCount.remove(tk);
							continue;
						}
						
						// Cache new record and dump trace file
						if (!onlyDumpNeeded && !detectionDetails.containsKey(record.pcid)) {
							detectionDetails.put(record.pcid, "detectionDetails/details_pcid" + record.pcid + ".txt");
							detectionReport.put(record.pcid, "detectionReport/report_pcid" + record.pcid + ".txt");
						}
						if (!dumpFile.containsKey(tk)) {
							String newFile = "dumpFile/tid" + record.tid + "_pid" + record.pid + "_pcid"
									+ record.pcid + "_processName_" + tmpRecords.get(i).processName + ".txt";
							dumpFile.put(tk, new BufferedWriter(new FileWriter(newFile)));
						}
						if (!onlyDumpNeeded) cacheRecord(record);
						
						if (needDump) {
							BufferedWriter fw = dumpFile.get(tk);
							fw.write(record.eventType + " @ " + record.parameter);
							fw.newLine();
							fw.flush();
						}
					}
				} else {
					System.out.println("Unexpected message type: " + msg.getClass());
				}
				if (count % 10000 == 0 || count % 10001 == 0) {
					String currentTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
					System.out.println("Consumed " + count + " records!");
					dataLogFile.write("Consumed " + count + " records!" + "@" + currentTime);
					dataLogFile.newLine();
					dataLogFile.flush();
				}
			}
						
		} catch (JMSException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}

//	@SuppressWarnings("unused")
//	@Override
	/**
	 * Present start() function
	 * Data messages accepted are consist of a unfixed number of data record. 
	 * One message is either consist of 10000 data records or a fix time interval of accumulated data records of 1 thread.
	 * cacheRecord() only caches the data record. detect() is triggered every message is received to avoid detection delay. 
	 */
	public void start() {
		/**
		 * Set up connection. not related to my work.
		 */
		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://" + host + ":" + port+ "?jms.useCompression=true");

		Connection connection;
		try {
			/**
			 * Start to receive messages.
			 */
			connection = factory.createConnection(user, password);
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination dest = session.createTopic(destination);

			
//			BufferedWriter dataLogFile = new BufferedWriter(new FileWriter("log/data.log", false));			
//			BufferedWriter processThreadLogFile = new BufferedWriter(new FileWriter("log/process_thread.log", false));
			
			
			MessageConsumer consumer = session.createConsumer(dest);
			long start = System.currentTimeMillis();
			long count = 0;
			System.out.println("Waiting for messages...");
			
			while (true) {
				Message msg = null;
				try {
					msg = consumer.receive();
				} catch (Exception e) {
					System.out.println("Consumer reception problem.");
				}
				if (msg instanceof BytesMessage) {
					/**
					 * Decoding messages to get the corresponding data record according to the mapping provided by the collector design.
					 */
					Date now = new Date();
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					String dateStr = dateFormat.format(now);
					
					int datalen = (int) ((BytesMessage) msg).getBodyLength();
					TraceKey tk = null;
//					System.out.println(datalen);
					
					if(datalen >=5)
					{
//						System.out.println("Get a record."+ datalen);
						byte[] byteMessage = new byte[datalen];
						((BytesMessage) msg).readBytes(byteMessage);
//						System.out.println(datalen);
//						for(int i = 0;i<datalen;i++)
//							System.out.print(" "+ (int)getUnsignedByte(byteMessage[i]));
//						System.out.println();
						int pcid = getUnsignedByte(byteMessage[0]);
						int threadid = getUnsignedByte(byteMessage[3])*256+getUnsignedByte(byteMessage[4]);
						String tid = Integer.toString(threadid);
						int processid = getUnsignedByte(byteMessage[1])*256+getUnsignedByte(byteMessage[2]);
						String pid = Integer.toString(processid);
						
						if (tk == null) tk = new TraceKey(tid, pid, Integer.toString(pcid));
						
						for(int i = 5; i < datalen; i++) {
//							System.out.println(byteMessage[i] + " " +getUnsignedByte(byteMessage[i]));
//							System.out.println(signature1215[getUnsignedByte(byteMessage[i])]);
							/**
							 * Set up all probably useful fields in EventRecord_NormalTest. 
							 * For more information about EventRecord_Normal class. Check out the comments in that class.
							 */
							String tData = signature1215[getUnsignedByte(byteMessage[i])];
							EventRecord_NormalTest record = new EventRecord_NormalTest();
							record.eventType = tData.split(" @ ")[0];
							if(tData.split(" @ ").length == 1)
								record.parameter = "";
							else
								record.parameter = tData.split(" @ ")[1];
							record.tid = tid;
							record.pid = pid;
//							System.out.println(record.eventType+" "+record.parameter + " " + record.tid + " " + record.pid);
							record.absoluteTime = dateStr;
							record.originalOrder = -1;
							record.order = -1;
							record.pcid = Integer.toString(pcid);
							
							/**
							 * A special manipulation recommended by Chunlin.
							 */
							if (record.eventType.equals("ImageDCStart")) record.eventType = "ImageLoad";
							
							++count;
							
							/**
							 *  Elminate out-of-date thread.
							 */
							if (record.eventType.equals("ThreadEnd")) {
								if (traceKey2records.get(tk) != null) detect(tk);
								traceKey2records.remove(tk);
								traceKey2detector.remove(tk);
								if (dumpFile.get(tk) != null) dumpFile.get(tk).close();
								dumpFile.remove(tk);
								testCount.remove(tk);
								continue;
							}
							
							/**
							 * Set up Detection Details and Detection Report
							 */
							if (!detectionDetails.containsKey(record.pcid)) {
								detectionDetails.put(record.pcid, "detectionDetails/details_pcid" + record.pcid + ".txt");
								detectionReport.put(record.pcid, "detectionReport/report_pcid" + record.pcid + ".txt");
							}
							
							/**
							 *  Cache new record and dump trace file
							 */
							if (!dumpFile.containsKey(tk)) {
								String newFile = "dumpFile/tid" + record.tid + "_pid" + record.pid + "_pcid"
										+ record.pcid + ".txt";
								dumpFile.put(tk, new BufferedWriter(new FileWriter(newFile)));
							}
							cacheRecord(record);
							
							if (needDump) {
								BufferedWriter fw = dumpFile.get(tk);
								fw.write(record.eventType + " @ " + record.parameter);
								fw.newLine();
								fw.flush();
							}
						}
						if (traceKey2records.containsKey(tk) && !traceKey2records.get(tk).isEmpty())
							detect(tk);
					}
					String currentTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
//					System.out.println("Consumed " + count + " records!");
				}
			}
		} catch (JMSException | InstantiationException | IllegalAccessException | IllegalArgumentException
			| InvocationTargetException | NoSuchMethodException | SecurityException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	
	}
	
	public int getUnsignedByte(byte data) { // 将data字节型数据转换为0~255 (0xFF 即BYTE)。
		return data & 0x0FF;
	}

	// private EventRecord convertETWJson2Object(String json){
	//// System.out.println(json);
	// return new Gson().fromJson(json, EventRecord.class);
	// }

	/**
	 * cache the event records according to thread_process_client.
	 * @param record
	 */
	private void cacheRecord(EventRecord_NormalTest record) throws IOException, InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		// cache the record
		TraceKey tk = new TraceKey(record.tid, record.pid, record.pcid);
		if (!traceKey2records.containsKey(tk)) {
			traceKey2records.put(tk, new ArrayList<EventRecord_NormalTest>());
		}
		traceKey2records.get(tk).add(record);
		if (!currentReceivedNumber.containsKey(tk)) {
			currentReceivedNumber.put(tk, 0);
		}
		currentReceivedNumber.put(tk, currentReceivedNumber.get(tk) + 1);

		// if up to the MaxCache Number,
//		if (currentReceivedNumber.get(tk) % MaxCacheNumber == 0) {
//			detect(tk);
//		}
	}
	
	/**
	 * do the real detection work.
	 * phf detection and initialization detection are split.
	 * @param tk
	 */
	private void detect(TraceKey tk) throws InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException, IOException {
		/**
		 *  preprocessing
		 */
		List<EventRecord_NormalTest> phfpreprocessed = Utils.phfTracePreProcess(traceKey2records.get(tk), minLengthOfConsecutiveBlock, maxLengthOfConsecutiveBlock, numberOfBlocksToKeep, true);
		List<EventRecord_NormalTest> initpreprocessed = Utils.initTracePreProcess(traceKey2records.get(tk), true);
		/**
		 * testCount is counting the index of pre-processed trace. It will be assigned to be EventRecord_NormalTest.order field.
		 * For preventing it from being too large(larger than the maximum value of int), it got module by 62500.
		 */
		if (!testCount.containsKey(tk))
			testCount.put(tk, 0);
		for (EventRecord_NormalTest record : phfpreprocessed) {
			record.order = testCount.get(tk);
			testCount.put(tk, (testCount.get(tk) + 1) % 62500);
		}
		
		/**
		 *  start matching
		 */
		if (!this.traceKey2detector.containsKey(tk)) {
			Detector newDetector = this.detector.getClass().getConstructor(TraceKey.class).newInstance(tk);
			newDetector.setPublisher(this.detector.getPublisher());
			this.traceKey2detector.put(tk, newDetector);
		}
		phfpreprocessed.forEach(r -> this.traceKey2detector.get(tk).inputPHF(r));
		initpreprocessed.forEach(r -> this.traceKey2detector.get(tk).inputInit(r));
		
		/**
		 *  reset
		 */
		traceKey2records.get(tk).clear();
	}

	/**
	 * old encoding mapping
	 */
	private String[] decodeList = { "@ExecutableFile",
			"0",
			"2",
			"3",
			"4",
			"5",
			"6",
			"12",
			"18",
			"20",
			"advapi32.dl",
			"apisetschema.dl",
			"AppCertDlls",
			"AppCompat",
			"AppCompatibility",
			"apphelp.dl",
			"AUDIOSES.DL",
			"AuthenticodeEnabled",
			"avicap32.dl",
			"AVRT.dl",
			"cfgmgr32.dl",
			"clbcatq.dl",
			"CodeIdentifiers",
			"comctl32.dl",
			"crypt32.dl",
			"cryptbase.dl",
			"DevicePath",
			"devobj.dl",
			"dhcpcsvc.dl",
			"dhcpcsvc6.dl",
			"DisableLocalOverride",
			"dnsapi.dl",
			"Domain",
			"DRIVERS32",
			"dwmapi.dl",
			"FWPUCLNT.DL",
			"gdi32.dl",
			"imm32.dl",
			"IPHLPAPI.DL",
			"KernelBase.dl",
			"ksuser.dl",
			"Layers",
			"lpk.dl",
			"midi",
			"MIDIMap",
			"midimap.dl",
			"midimapper",
			"MMDevAPI.DL",
			"msacm32.dl",
			"msasn1.dl",
			"mscorlib.ni.dl",
			"mscorwks.dl",
			"msctf.dl",
			"msvcrt.dl",
			"msvfw32.dl",
			"mswsock.dl",
			"NapiNSP.dl",
			"netapi32.dl",
			"nsi.dl",
			"NtAllocateVirtualMemory",
			"NtAlpcCreateSecurityContext",
			"NtApphelpCacheContro",
			"NtCallbackReturn",
			"NtCreateFile",
			"NtCreateNamedPipeFile",
			"NtCreateSection",
			"NtCreateThreadEx",
			"NtCreateUserProcess",
			"ntdll.dl",
			"NtFlushInstructionCache",
			"NtFlushProcessWriteBuffers",
			"NtFreeVirtualMemory",
			"NtFsControlFile",
			"NtGdiBitBlt",
			"NtGdiCreateCompatibleBitmap",
			"NtGdiCreateCompatibleDC",
			"NtGdiCreateDIBSection",
			"NtGdiDeleteObjectApp",
			"NtGdiExtGetObjectW",
			"NtGdiFlush",
			"NtGdiGetDeviceCaps",
			"NtGdiGetDIBitsInterna",
			"NtGdiOpenDCW",
			"NtGdiSelectBitmap",
			"NtGdiStretchBlt",
			"NtMapViewOfSection",
			"NtOpenFile",
			"NtOpenKey",
			"NtOpenKeyEx",
			"NtOpenSection",
			"NtProtectVirtualMemory",
			"NtQueryAttributesFile",
			"NtQueryDebugFilterState",
			"NtQueryDirectoryFile",
			"NtQueryInformationProcess",
			"NtQueryKey",
			"NtQueryObject",
			"NtQuerySection",
			"NtQuerySystemInformation",
			"NtQueryValueKey",
			"NtQueryVirtualMemory",
			"NtReadFile",
			"NtResumeThread",
			"NtSetInformationFile",
			"NtSetInformationKey",
			"NtSetInformationThread",
			"NtSetTimer",
			"NtTerminateProcess",
			"NtTerminateThread",
			"NtUnmapViewOfSection",
			"NtUserCloseClipboard",
			"NtUserCreateLocalMemHandle",
			"NtUserCreateWindowEx",
			"NtUserDestroyWindow",
			"NtUserGetAsyncKeyState",
			"NtUserGetClipboardData",
			"NtUserGetDC",
			"NtUserGetForegroundWindow",
			"NtUserGetKeyboardState",
			"NtUserGetKeyState",
			"NtUserGetProp",
			"NtUserMapVirtualKeyEx",
			"NtUserOpenClipboard",
			"NtUserQueryWindow",
			"NtUserRemoveProp",
			"NtUserSetProp",
			"NtUserSetWindowLongPtr",
			"NtUserToUnicodeEx",
			"NtWaitForWorkViaWorkerFactory",
			"NtWriteFile",
			"NtWriteVirtualMemory",
			"ole32.dl",
			"oleaut32.dl",
			"Option",
			"Parameters",
			"pnrpnsp.dl",
			"PreferExternalManifest",
			"Properties",
			"PROPSYS.dl",
			"rasadhlp.dl",
			"rpcrt4.dl",
			"rtutils.dl",
			"sechost.dl",
			"setupapi.dl",
			"shell32.dl",
			"shfolder.dl",
			"shlwapi.dl",
			"SideBySide",
			"SourcePath",
			"SQMServiceList",
			"srvcli.dlla",
			"sspicli.dl",
			"SwDRM.dl",
			"System.Configuration.ni.dl",
			"System.ni.dl",
			"System.Windows.Forms.ni.dl",
			"System.Xml.ni.dl",
			"SystemSetupInProgress",
			"SysWOW64",
			"TransparentEnabled",
			"urlmon.dl",
			"usp10.dl",
			"uxtheme.dl",
			"version.dl",
			"wave",
			"wavemapper",
			"whee",
			"Winmm",
			"winmm.dl",
			"winnsi.dl",
			"winrnr.dl",
			"wkscli.dl",
			"Wldap32.dl",
			"WMINet_Utils.dl",
			"wow64.dl",
			"wow64cpu.dl",
			"wow64win.dl",
			"ws2_32.dl",
			"wshbth.dl",
			"WSHTCPIP.DL",
			"wsock32.dl",
			"@DataFile",
			"@DeviceFile",
			"" };

	/**
	 * new encoding mapping
	 */
	private String[] signature1215 = {
			"ALPCALPC-Receive-Message @ ProcessName:csrss.exe",
			"ALPCALPC-Receive-Message @ ProcessName:dwm.exe",
			"ALPCALPC-Send-Message @ ProcessName:",
			"ALPCALPC-Send-Message @ ProcessName:csrss.exe",
			"ALPCALPC-Send-Message @ ProcessName:dwm.exe",
			"ALPCALPC-Unwait",
			"ALPCALPC-Wait-For-Reply",
			"FileIoCleanup @ FileName:@DataFile",
			"FileIoCleanup @ FileName:cmd.exe",
			"FileIoCleanup @ FileName:HarddiskVolume1",
			"FileIoCleanup @ FileName:RATs",
			"FileIoCleanup @ FileName:system32",
			"FileIoCleanup @ FileName:SysWOW64",
			"FileIoCleanup @ FileName:Windows",
			"FileIoCleanup @ FileName:XXX.temp",
			"FileIoClose @ FileName:@DataFile",
			"FileIoClose @ FileName:cmd.exe",
			"FileIoClose @ FileName:HarddiskVolume1",
			"FileIoClose @ FileName:RATs",
			"FileIoClose @ FileName:system32",
			"FileIoClose @ FileName:SysWOW64",
			"FileIoClose @ FileName:Windows",
			"FileIoClose @ FileName:XXX.temp",
			"FileIoCreate @ FileName:@DataFile",
			"FileIoCreate @ FileName:cmd.exe",
			"FileIoCreate @ FileName:HarddiskVolume1",
			"FileIoCreate @ FileName:RATs",
			"FileIoCreate @ FileName:SwDRM.dll",
			"FileIoCreate @ FileName:system32",
			"FileIoCreate @ FileName:SysWOW64",
			"FileIoCreate @ FileName:Windows",
			"FileIoCreate @ FileName:XXX.temp",
			"FileIoDirEnum @ FileName:HarddiskVolume1",
			"FileIoDirEnum @ FileName:system32",
			"FileIoDirEnum @ FileName:SysWOW64",
			"FileIoDirEnum @ FileName:Windows",
			"FileIoFileCreate @ FileName:XXX.temp",
			"FileIoRead @ FileName:cmd.exe",
			"FileIoWrite @ FileName:XXX.temp",
			"ImageLoad @ FileName:cmd.exe",
			"ImageUnLoad @ FileName:cmd.exe",
			"PerfInfoSysClEnter @ SystemCall:NtApphelpCacheControl",
			"PerfInfoSysClEnter @ SystemCall:NtCallbackReturn",
			"PerfInfoSysClEnter @ SystemCall:NtCreateFile",
			"PerfInfoSysClEnter @ SystemCall:NtCreateNamedPipeFile",
			"PerfInfoSysClEnter @ SystemCall:NtCreateSection",
			"PerfInfoSysClEnter @ SystemCall:NtCreateThreadEx",
			"PerfInfoSysClEnter @ SystemCall:NtCreateUserProcess",
			"PerfInfoSysClEnter @ SystemCall:NtDeviceIoControlFile",
			"PerfInfoSysClEnter @ SystemCall:NtFlushInstructionCache",
			"PerfInfoSysClEnter @ SystemCall:NtFlushProcessWriteBuffers",
			"PerfInfoSysClEnter @ SystemCall:NtFsControlFile",
			"PerfInfoSysClEnter @ SystemCall:NtGdiBitBlt",
			"PerfInfoSysClEnter @ SystemCall:NtGdiCreateCompatibleBitmap",
			"PerfInfoSysClEnter @ SystemCall:NtGdiCreateCompatibleDC",
			"PerfInfoSysClEnter @ SystemCall:NtGdiCreateDIBSection",
			"PerfInfoSysClEnter @ SystemCall:NtGdiCreatePaletteInternal",
			"PerfInfoSysClEnter @ SystemCall:NtGdiDeleteObjectApp",
			"PerfInfoSysClEnter @ SystemCall:NtGdiExtGetObjectW",
			"PerfInfoSysClEnter @ SystemCall:NtGdiFlush",
			"PerfInfoSysClEnter @ SystemCall:NtGdiGetDCDword",
			"PerfInfoSysClEnter @ SystemCall:NtGdiGetDCObject",
			"PerfInfoSysClEnter @ SystemCall:NtGdiGetDeviceCaps",
			"PerfInfoSysClEnter @ SystemCall:NtGdiGetDIBitsInternal",
			"PerfInfoSysClEnter @ SystemCall:NtGdiOpenDCW",
			"PerfInfoSysClEnter @ SystemCall:NtGdiPatBlt",
			"PerfInfoSysClEnter @ SystemCall:NtGdiSelectBitmap",
			"PerfInfoSysClEnter @ SystemCall:NtGdiSetDIBitsToDeviceInternal",
			"PerfInfoSysClEnter @ SystemCall:NtGdiStretchBlt",
			"PerfInfoSysClEnter @ SystemCall:NtMapViewOfSection",
			"PerfInfoSysClEnter @ SystemCall:NtOpenFile",
			"PerfInfoSysClEnter @ SystemCall:NtOpenKey",
			"PerfInfoSysClEnter @ SystemCall:NtOpenKeyEx",
			"PerfInfoSysClEnter @ SystemCall:NtProtectVirtualMemory",
			"PerfInfoSysClEnter @ SystemCall:NtQueryAttributesFile",
			"PerfInfoSysClEnter @ SystemCall:NtQueryDebugFilterState",
			"PerfInfoSysClEnter @ SystemCall:NtQueryDirectoryFile",
			"PerfInfoSysClEnter @ SystemCall:NtQueryInformationProcess",
			"PerfInfoSysClEnter @ SystemCall:NtQueryLicenseValue",
			"PerfInfoSysClEnter @ SystemCall:NtQueryObject",
			"PerfInfoSysClEnter @ SystemCall:NtQuerySystemInformation",
			"PerfInfoSysClEnter @ SystemCall:NtQueryValueKey",
			"PerfInfoSysClEnter @ SystemCall:NtQueryVirtualMemory",
			"PerfInfoSysClEnter @ SystemCall:NtReadFile",
			"PerfInfoSysClEnter @ SystemCall:NtReadVirtualMemory",
			"PerfInfoSysClEnter @ SystemCall:NtRemoveIoCompletion",
			"PerfInfoSysClEnter @ SystemCall:NtResumeThread",
			"PerfInfoSysClEnter @ SystemCall:NtSetInformationFile",
			"PerfInfoSysClEnter @ SystemCall:NtSetInformationKey",
			"PerfInfoSysClEnter @ SystemCall:NtSetInformationThread",
			"PerfInfoSysClEnter @ SystemCall:NtTerminateProcess",
			"PerfInfoSysClEnter @ SystemCall:NtTerminateThread",
			"PerfInfoSysClEnter @ SystemCall:NtUnmapViewOfSection",
			"PerfInfoSysClEnter @ SystemCall:NtUserBuildHwndList",
			"PerfInfoSysClEnter @ SystemCall:NtUserCallNextHookEx",
			"PerfInfoSysClEnter @ SystemCall:NtUserDrawIconEx",
			"PerfInfoSysClEnter @ SystemCall:NtUserGetAsyncKeyState",
			"PerfInfoSysClEnter @ SystemCall:NtUserGetDC",
			"PerfInfoSysClEnter @ SystemCall:NtUserGetForegroundWindow",
			"PerfInfoSysClEnter @ SystemCall:NtUserGetKeyboardState",
			"PerfInfoSysClEnter @ SystemCall:NtUserGetKeyNameText",
			"PerfInfoSysClEnter @ SystemCall:NtUserGetKeyState",
			"PerfInfoSysClEnter @ SystemCall:NtUserGetMessage",
			"PerfInfoSysClEnter @ SystemCall:NtUserGetRawInputData",
			"PerfInfoSysClEnter @ SystemCall:NtUserGetWindowDC",
			"PerfInfoSysClEnter @ SystemCall:NtUserMapVirtualKeyEx",
			"PerfInfoSysClEnter @ SystemCall:NtUserPeekMessage",
			"PerfInfoSysClEnter @ SystemCall:NtUserQueryWindow",
			"PerfInfoSysClEnter @ SystemCall:NtUserRegisterRawInputDevices",
			"PerfInfoSysClEnter @ SystemCall:NtUserSetWindowsHookEx",
			"PerfInfoSysClEnter @ SystemCall:NtUserToUnicodeEx",
			"PerfInfoSysClEnter @ SystemCall:NtUserWaitMessage",
			"PerfInfoSysClEnter @ SystemCall:NtWaitForMultipleObjects32",
			"PerfInfoSysClEnter @ SystemCall:NtWriteFile",
			"PerfInfoSysClEnter @ SystemCall:NtWriteVirtualMemory",
			"ProcessStart @ ImageFileName:cmd.exe @@ CommandLine:cmd.exe",
			"RegistryOpen @ KeyName:cmd.exe",
			"RegistryOpen @ KeyName:Layers",
			"RegistryOpen @ KeyName:Shell Folders",
			"RegistryOpen @ KeyName:SideBySide",
			"RegistryQueryValue @ KeyName:Cache",
			"RegistryQueryValue @ KeyName:PreferExternalManifest",
			"RegistrySetInformation @ KeyName:UNKNOWN_KEY",
			"ThreadEnd @ ",
	};
	
}
