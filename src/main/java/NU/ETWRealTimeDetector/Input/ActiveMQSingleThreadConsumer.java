/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package NU.ETWRealTimeDetector.Input;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import NU.ETWRealTimeDetector.EventRecord_NormalTest;
import NU.ETWRealTimeDetector.EventRecord_FPTest;
import NU.ETWRealTimeDetector.Utils;
import NU.ETWRealTimeDetector.Commucation.SendToSchdular;
import NU.ETWRealTimeDetector.Match.Detector;
import NU.ETWRealTimeDetector.Match.TraceKey;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.jms.*;

public class ActiveMQSingleThreadConsumer extends Consumer {
	
	public static int numberOfBlocksToKeep = 2;
	public static int minLengthOfConsecutiveBlock = 1;
	public static int maxLengthOfConsecutiveBlock = 500;

	protected String user;
	protected String password;
	protected String url;
	protected String destination;

	public static int MaxCacheNumber = 10000;
	protected int maxBlockSize = 500;
	protected boolean needDump = false;
	
	/**
	 * All data will be used for detection related to one single thread.
	 */
	protected Map<TraceKey, List<EventRecord_NormalTest>> traceKey2records = new HashMap<TraceKey, List<EventRecord_NormalTest>>();
	protected Map<TraceKey, Detector> traceKey2detector = new HashMap<TraceKey, Detector>();
	protected Map<TraceKey, Integer> currentReceivedNumber = new HashMap<TraceKey, Integer>();
	protected Map<TraceKey, Integer> currentRoundNumber = new HashMap<TraceKey, Integer>();
	protected Map<TraceKey, Integer> testCount = new HashMap<TraceKey, Integer>();
	protected Map<TraceKey, Long> lastDetectionTime = new HashMap<TraceKey, Long>(); //This should be canceled in next version, because it is done by data collector.
	protected Map<TraceKey, BufferedWriter> dumpFile = new HashMap<TraceKey, BufferedWriter>();
	
	Map<Integer, String> opcode_eventtype_map = new HashMap<Integer, String>();
	
	public static Map<String, String> detectionDetails = new HashMap<String, String>();
	public static Map<String, String> detectionReport = new HashMap<String, String>();
	
	protected SendToSchdular communication;

	/**
	 * The Consumer class for pratical online detector. Integrated with scheduler authored by Liheng but not directly integrated with Chunlin's newest collector 
	 * The constructor below is to set up several connection information it will get from ActiveMQSingleThreadConsumerMain.
	 * They define a specific ActiveMQ data tunnel used by the consumer. 
	 * @param machinename
	 * @param user
	 * @param password
	 * @param url
	 * @param destination
	 * @param needDump
	 * @throws IOException
	 */
	public ActiveMQSingleThreadConsumer(String machinename, String user, String password, String url,  String destination, boolean needDump) throws IOException {
		this.user = user;
		this.password = password;
		// host = "localhost";
		// host = "10.214.148.122";
		this.url = url;
		this.destination = destination;
		this.needDump = needDump;
		communication = new SendToSchdular(machinename, user, password, url, destination);
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	@SuppressWarnings("unused")
	@Override
	/**
	 * start() function for ActiveMQSingleThreadConsumer class
	 * Basically corresponding to oldStart() in ActiveMQConsumer class
	 * Data messages accepted are consist of a fixed number(10000 for partial-kernel mode, cuz it only has partial-kernel mode) of data record from all threads, all processes and all client hosts.
	 * Some logics are also not matched to present collector design(It needs cacheRecord() to be in charge of triggering detect(). detect() is triggered when it gets 10000 pre-processed records). 
	 * Basically of no use if no modification is done to match the new collector design.
	 */
	public void start() {
		opcode_eventtype_map.put(33, "ALPC_SEN");
		opcode_eventtype_map.put(32, "FileIoCreate");
		opcode_eventtype_map.put(64, "FileIoCreate");
		opcode_eventtype_map.put(74, "FileIoQueryInfo");
		opcode_eventtype_map.put(67, "FileIoRead");
		opcode_eventtype_map.put(68, "FileIoWrite");
		opcode_eventtype_map.put(3, "ImageDCStart");
		opcode_eventtype_map.put(22, "RegistryKCBCreate");
		opcode_eventtype_map.put(11, "RegistryOpen");
		opcode_eventtype_map.put(13, "RegistryQuery");
		opcode_eventtype_map.put(16, "RegistryQueryValue");
		opcode_eventtype_map.put(20, "RegistrySetInformation");
		opcode_eventtype_map.put(51, "SystemCall");
		opcode_eventtype_map.put(110, "ImageLoad");
		opcode_eventtype_map.put(10, "RegistryCreate");
		opcode_eventtype_map.put(34, "ALPC_REC");
		opcode_eventtype_map.put(134, "DiskIoDrvMjFnCall");
		opcode_eventtype_map.put(2, "ThreadEnd");
		opcode_eventtype_map.put(21, "ProcessEnd");
		Connection connection=null;
		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(this.user,this.password,this.url);		
		try {
			connection = factory.createConnection();
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination dest = session.createTopic(destination);

			
			BufferedWriter dataLogFile = new BufferedWriter(new FileWriter("log/data.log", false));			
			BufferedWriter processThreadLogFile = new BufferedWriter(new FileWriter("log/process_thread.log", false));
			
			
			MessageConsumer consumer = session.createConsumer(dest);
			long start = System.currentTimeMillis();
			long count = 0;
			System.out.println("Waiting for messages...");
			
			long lastYield = System.currentTimeMillis();
			
			while (true) {
				Message msg = null;
				try {
					msg = consumer.receive();
				} catch (Exception e) {
					System.out.println("Consumer reception problem.");
				}
				if (msg instanceof BytesMessage) {
					byte[] byteMessage = new byte[90100];
					((BytesMessage) msg).readBytes(byteMessage);
					Date now = new Date();
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					String dateStr = dateFormat.format(now);
					/**
					 * Go along all 10000 records. Decode them and cache them.
					 */
					for (int i = 0; i < 10000; i++) {
						/**
						 * Decoding.
						 */
						int countNumber = getUnsignedByte(byteMessage[i * 9 + 1]) - 1
								+ (getUnsignedByte(byteMessage[i * 9 + 2]) - 1) * 255;
						int pid = getUnsignedByte(byteMessage[i * 9 + 3]) - 1
								+ (getUnsignedByte(byteMessage[i * 9 + 4]) - 1) * 255;
						int tid = getUnsignedByte(byteMessage[i * 9 + 5]) - 1
								+ (getUnsignedByte(byteMessage[i * 9 + 6]) - 1) * 255;
						int parameterNo = getUnsignedByte(byteMessage[i * 9 + 7]) - 1
								+ (getUnsignedByte(byteMessage[i * 9 + 8]) - 1) * 255;
						int pcid = getUnsignedByte(byteMessage[0]);
						String parameter = decodeList[parameterNo];
						int opcode = getUnsignedByte(byteMessage[i * 9 + 9]) - 1;
						String eventType = "";
						if (opcode_eventtype_map.containsKey(opcode))
							eventType = opcode_eventtype_map.get(opcode);
						else
							System.out.println(opcode);
						if(opcode == 2){
//							System.out.println(eventType + " " + threadid);							
							String currentTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
							processThreadLogFile.write(eventType + "@" + tid + "@" + pcid + "@" + currentTime);
							processThreadLogFile.newLine();
							processThreadLogFile.flush();
						}
						else if(opcode == 21){
//							System.out.println(eventType + " " + pid);
							String currentTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
							processThreadLogFile.write(eventType + "@" + pid + "@" + pcid + "@" + currentTime);
							processThreadLogFile.newLine();
							processThreadLogFile.flush();
						}
						/**
						 * A special manipulation recommended by Chunlin.
						 */
						if (eventType.equals("ImageDCStart")) eventType = "ImageLoad";
						/**
						 * Transfer to EventRecord_NormalTest for further use.
						 */
						EventRecord_NormalTest record = new EventRecord_NormalTest();
						record.pid = Integer.toString(pid);
						record.absoluteTime = dateStr;
						record.tid = Integer.toString(tid);
						record.eventType = eventType;
						record.parameter = parameter;
						record.originalOrder = countNumber;
						record.order = -1;
						record.pcid = Integer.toString(pcid);
						TraceKey tk = new TraceKey(record.tid, record.pid, record.pcid);
						++count;
						/**
						 * 4 different control-related events. As requirement from Liheng, it is reported to Scheduler using corresponding API in communication.
						 * Among them, ThreadEnd events are designed for eliminating out-of-date thread in the whole detection system.
						 */
						if (record.eventType.equals("ThreadStart")) {
							communication.SendThreadCreateToSchdular(tid, pid);
						} else
						if (record.eventType.equals("ProcessStart")) {
							String[] splitResult = record.parameter.split(" @@ ");
							String[] splitResult2 = splitResult[0].split(":");
							String path = splitResult2[1];
							splitResult2 = splitResult[1].split(":");
							String commandline = splitResult2[1];
							communication.SendProcessCreateToSchdular(pid, path, commandline);
						} else
						/**
						 *  Elminate out-of-date thread.
						 */
						if (record.eventType.equals("ThreadEnd")) {
							if (traceKey2records.get(tk) != null) detect(tk);
							traceKey2records.remove(tk);
							traceKey2detector.remove(tk);
							if (dumpFile.get(tk) != null) dumpFile.get(tk).close();
							dumpFile.remove(tk);
							testCount.remove(tk);
							communication.SendThreadEndToSchdular(tid);
							continue;
						} else
						if (record.eventType.equals("ProcessEnd")) {
							communication.SendProcessEndToSchdular(pid);
						}
						
						/**
						 * Set up Detection Details and Detection Report
						 */
						if (!detectionDetails.containsKey(record.pcid)) {
							detectionDetails.put(record.pcid, "detectionDetails/details_pcid" + record.pcid + ".txt");
							detectionReport.put(record.pcid, "detectionReport/report_pcid" + record.pcid + ".txt");
						}
						
						/**
						 * Cache new record and dump trace file
						 */
						if (!dumpFile.containsKey(tk)) {
							String newFile = "dumpFile/tid" + record.tid + "_pid" + record.pid + "_pcid"
									+ record.pcid + ".txt";
							dumpFile.put(tk, new BufferedWriter(new FileWriter(newFile)));
						}
						cacheRecord(record);
						
						if (needDump) {
							BufferedWriter fw = dumpFile.get(tk);
							fw.write(record.eventType + " @ " + record.parameter);
							fw.newLine();
							fw.flush();
						}
					}
					/**
					 * Time detection triggering mechanism.
					 * Used for reduce delay between phf essential event received by detector and detector reporting the phf.
					 */
					for (TraceKey tk : traceKey2records.keySet()) 
					if (new Date().getTime() - lastDetectionTime.get(tk) > 120000){
						lastDetectionTime.put(tk, new Date().getTime());
						System.out.println("Activated on " + lastDetectionTime.get(tk));
						detect(tk);
					}
				} else {
					System.out.println("Unexpected message type: " + msg.getClass());
				}
				if (count % 10000 == 0 || count % 10001 == 0) {
					String currentTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
					System.out.println("Consumed " + count + " records!");
					dataLogFile.write("Consumed " + count + " records!" + "@" + currentTime);
					dataLogFile.newLine();
					dataLogFile.flush();
				}
				
				/**
				 * Thread yielding part.
				 * Used for thread termination control from Scheduler.
				 */
				long now = System.currentTimeMillis();
				if (now - lastYield >= 10000) {
					lastYield = now;
					Thread t = new Thread();
					try {
						t.currentThread().sleep(1);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
						
		} catch (JMSException | InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}

	public int getUnsignedByte(byte data) { // 灏哾ata瀛楄妭鍨嬫暟鎹浆鎹负0~255 (0xFF 鍗矪YTE)銆�
		return data & 0x0FF;
	}

	// private EventRecord convertETWJson2Object(String json){
	//// System.out.println(json);
	// return new Gson().fromJson(json, EventRecord.class);
	// }
	
	/**
	 * cache the event records according to thread_process_client for further trace pre-processing.
	 * In charge of triggering detect()
	 * @param record
	 */
	private void cacheRecord(EventRecord_NormalTest record) throws IOException, InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		/**
		 *  cache the record
		 */
		TraceKey tk = new TraceKey(record.tid, record.pid, record.pcid);
		if (!traceKey2records.containsKey(tk)) {
			traceKey2records.put(tk, new ArrayList<EventRecord_NormalTest>());
		}
		traceKey2records.get(tk).add(record);
		if (!lastDetectionTime.containsKey(tk)) {
			lastDetectionTime.put(tk, new Date().getTime());
		}
		if (!currentReceivedNumber.containsKey(tk)) {
			currentReceivedNumber.put(tk, 0);
		}
		currentReceivedNumber.put(tk, currentReceivedNumber.get(tk) + 1);
		
		/**
		 *  if up to the MaxCache Number
		 */
		if (currentReceivedNumber.get(tk) % MaxCacheNumber == 0) {
			lastDetectionTime.put(tk, new Date().getTime());
			detect(tk);
		}
	}

	private void detect(TraceKey tk) throws InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException, IOException {
		/**
		 *  preprocessing
		 */
		List<EventRecord_NormalTest> preprocessed = Utils.phfTracePreProcess(traceKey2records.get(tk), minLengthOfConsecutiveBlock, maxLengthOfConsecutiveBlock, numberOfBlocksToKeep, true);
		if (!testCount.containsKey(tk))
			testCount.put(tk, 0);
		for (EventRecord_NormalTest record : preprocessed) {
			record.order = testCount.get(tk);
			testCount.put(tk, (testCount.get(tk) + 1) % 62500);
		}
		traceKey2records.put(tk, preprocessed);
		/**
		 *  start matching
		 */
		if (!this.traceKey2detector.containsKey(tk)) {
			Detector newDetector = this.detector.getClass().getConstructor(TraceKey.class).newInstance(tk);
			newDetector.setPublisher(this.detector.getPublisher());
			newDetector.setCommunication(communication);
			this.traceKey2detector.put(tk, newDetector);
		}
		this.traceKey2records.get(tk).forEach(r -> this.traceKey2detector.get(tk).inputPHF(r));
		/**
		 *  reset
		 */
		traceKey2records.get(tk).clear();
	}

	private String[] decodeList = { "@ExecutableFile",
			"0",
			"2",
			"3",
			"4",
			"5",
			"6",
			"12",
			"18",
			"20",
			"advapi32.dll",
			"apisetschema.dll",
			"AppCertDlls",
			"AppCompat",
			"AppCompatibility",
			"apphelp.dll",
			"AUDIOSES.DLL",
			"AuthenticodeEnabled",
			"avicap32.dll",
			"AVRT.dll",
			"cfgmgr32.dll",
			"clbcatq.dll",
			"CodeIdentifiers",
			"comctl32.dll",
			"crypt32.dll",
			"cryptbase.dll",
			"DevicePath",
			"devobj.dll",
			"dhcpcsvc.dll",
			"dhcpcsvc6.dll",
			"DisableLocalOverride",
			"dnsapi.dll",
			"Domain",
			"DRIVERS32",
			"dwmapi.dll",
			"FWPUCLNT.DLL",
			"gdi32.dll",
			"imm32.dll",
			"IPHLPAPI.DLL",
			"KernelBase.dll",
			"ksuser.dll",
			"Layers",
			"lpk.dll",
			"midi",
			"MIDIMap",
			"midimap.dll",
			"midimapper",
			"MMDevAPI.DLL",
			"msacm32.dll",
			"msasn1.dll",
			"mscorlib.ni.dll",
			"mscorwks.dll",
			"msctf.dll",
			"msvcrt.dll",
			"msvfw32.dll",
			"mswsock.dll",
			"NapiNSP.dll",
			"netapi32.dll",
			"nsi.dll",
			"NtAllocateVirtualMemory",
			"NtAlpcCreateSecurityContext",
			"NtApphelpCacheControl",
			"NtCallbackReturn",
			"NtCreateFile",
			"NtCreateNamedPipeFile",
			"NtCreateSection",
			"NtCreateThreadEx",
			"NtCreateUserProcess",
			"ntdll.dll",
			"NtFlushInstructionCache",
			"NtFlushProcessWriteBuffers",
			"NtFreeVirtualMemory",
			"NtFsControlFile",
			"NtGdiBitBlt",
			"NtGdiCreateCompatibleBitmap",
			"NtGdiCreateCompatibleDC",
			"NtGdiCreateDIBSection",
			"NtGdiDeleteObjectApp",
			"NtGdiExtGetObjectW",
			"NtGdiFlush",
			"NtGdiGetDeviceCaps",
			"NtGdiGetDIBitsInternal",
			"NtGdiOpenDCW",
			"NtGdiSelectBitmap",
			"NtGdiStretchBlt",
			"NtMapViewOfSection",
			"NtOpenFile",
			"NtOpenKey",
			"NtOpenKeyEx",
			"NtOpenSection",
			"NtProtectVirtualMemory",
			"NtQueryAttributesFile",
			"NtQueryDebugFilterState",
			"NtQueryDirectoryFile",
			"NtQueryInformationProcess",
			"NtQueryKey",
			"NtQueryObject",
			"NtQuerySection",
			"NtQuerySystemInformation",
			"NtQueryValueKey",
			"NtQueryVirtualMemory",
			"NtReadFile",
			"NtResumeThread",
			"NtSetInformationFile",
			"NtSetInformationKey",
			"NtSetInformationThread",
			"NtSetTimer",
			"NtTerminateProcess",
			"NtTerminateThread",
			"NtUnmapViewOfSection",
			"NtUserCloseClipboard",
			"NtUserCreateLocalMemHandle",
			"NtUserCreateWindowEx",
			"NtUserDestroyWindow",
			"NtUserGetAsyncKeyState",
			"NtUserGetClipboardData",
			"NtUserGetDC",
			"NtUserGetForegroundWindow",
			"NtUserGetKeyboardState",
			"NtUserGetKeyState",
			"NtUserGetProp",
			"NtUserMapVirtualKeyEx",
			"NtUserOpenClipboard",
			"NtUserQueryWindow",
			"NtUserRemoveProp",
			"NtUserSetProp",
			"NtUserSetWindowLongPtr",
			"NtUserToUnicodeEx",
			"NtWaitForWorkViaWorkerFactory",
			"NtWriteFile",
			"NtWriteVirtualMemory",
			"ole32.dll",
			"oleaut32.dll",
			"Option",
			"Parameters",
			"pnrpnsp.dll",
			"PreferExternalManifest",
			"Properties",
			"PROPSYS.dll",
			"rasadhlp.dll",
			"rpcrt4.dll",
			"rtutils.dll",
			"sechost.dll",
			"setupapi.dll",
			"shell32.dll",
			"shfolder.dll",
			"shlwapi.dll",
			"SideBySide",
			"SourcePath",
			"SQMServiceList",
			"srvcli.dlla",
			"sspicli.dll",
			"SwDRM.dll",
			"System.Configuration.ni.dll",
			"System.ni.dll",
			"System.Windows.Forms.ni.dll",
			"System.Xml.ni.dll",
			"SystemSetupInProgress",
			"SysWOW64",
			"TransparentEnabled",
			"urlmon.dll",
			"usp10.dll",
			"uxtheme.dll",
			"version.dll",
			"wave",
			"wavemapper",
			"wheel",
			"Winmm",
			"winmm.dll",
			"winnsi.dll",
			"winrnr.dll",
			"wkscli.dll",
			"Wldap32.dll",
			"WMINet_Utils.dll",
			"wow64.dll",
			"wow64cpu.dll",
			"wow64win.dll",
			"ws2_32.dll",
			"wshbth.dll",
			"WSHTCPIP.DLL",
			"wsock32.dll",
			"@DataFile",
			"@DeviceFile",
			"" };
}
