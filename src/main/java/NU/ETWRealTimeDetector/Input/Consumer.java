package NU.ETWRealTimeDetector.Input;

import NU.ETWRealTimeDetector.Match.Detector;

public abstract class Consumer {
	protected Detector detector;

	public abstract void start();

	public Detector getDetector() {
		return detector;
	}

	public void setDetector(Detector detector) {
		this.detector = detector;
	}

}
