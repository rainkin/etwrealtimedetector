package NU.ETWRealTimeDetector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class offer some utils tool used in real-time detection.
 * It have several general methods and overall high-level methods for PHF pre-processing and INIT pre-processing.
 * And those code are actually provided by Runqing.
 * Filter class and Utils class are both serving as some urgent tools in real-time detection, 
 * but Filter class contains more "general" data-driven algorithm and Utils class contains more "specific" manipulation with human heuristics. 
 * @author ander
 *
 * @param <T>
 */
public class Utils {

	/**
	 * Some executable files won't be generalized but remain its own name.
	 * Most of them are essential system-related executables.
	 */
	public static String[] specialExeList = { "cmd.exe", "svchost.exe", "shutdown.exe", "dwm.exe", "whoami.exe",
			"lsass.exe", "taskmgr.exe", "dir.exe", "taskhost.exe", "winupdate.exe", "csrss.exe", "SearchIndexer.exe",
			"services.exe", "audiodg.exe", "conhost.exe" };
	
	public static HashSet<String> phfUselessSyscallList;
	public static HashSet<String> initUselessSyscallList;
	
	/**
	 * Read in 2 useless system call lists for phf detection and initialization detetion.
	 */
	static {
		phfUselessSyscallList = new HashSet<String>();
		initUselessSyscallList = new HashSet<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader("importantList/useless_e4.txt"));
			String data;
			while ((data = br.readLine()) != null) {
				phfUselessSyscallList.add(data);
			}
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			BufferedReader br = new BufferedReader(new FileReader("importantList/useless_haitao_init.txt"));
			String data;
			while ((data = br.readLine()) != null) {
				initUselessSyscallList.add(data);
			}
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Overall method for phf trace pre-preprocessing
	 * It will eliminate phf-useless system calls and apply phf-related argument pre-processing and consecutive section reduction algorithm in Filter.java.
	 * @param sequence
	 * @param minLengthOfConsecutiveBlock
	 * @param maxLengthOfConsecutiveBlock
	 * @param numberOfBlocksToKeep
	 * @param onlineMode
	 * @return
	 * @throws IOException
	 */
	public static List<EventRecord_NormalTest> phfTracePreProcess(List<EventRecord_NormalTest> sequence,
			int minLengthOfConsecutiveBlock, int maxLengthOfConsecutiveBlock, int numberOfBlocksToKeep,
			boolean onlineMode) throws IOException {

		/**
		 *  preprocess arguments
		 */
		Iterator<EventRecord_NormalTest> i = sequence.iterator();
		List<EventRecord_NormalTest> resList = new ArrayList<EventRecord_NormalTest>();

		while (i.hasNext()) {
			EventRecord_NormalTest tmpRecord = new EventRecord_NormalTest(i.next());
			if (!onlineMode) {
				if (!tmpRecord.eventType.equals("PerfInfoSysClEnter")) {
					String[] splitResult;
					if (tmpRecord.eventType.equals("ProcessStart") || tmpRecord.eventType.equals("ProcessEnd")) {
						String[] splitResult2 = tmpRecord.parameter.split(" @@ ");
						splitResult = splitResult2[0].split(":");
						String part1;
						if (splitResult.length >= 2)
							part1 = splitResult[0] + ":" + argPreProcessPHF(splitResult[1]);
						else
							part1 = splitResult[0] + ":";
						splitResult = splitResult2[1].split(":");
						String part2;
						if (splitResult.length >= 2)
							part2 = splitResult[0] + ":" + argPreProcessPHF(splitResult[1]);
						else
							part2 = splitResult[0] + ":";
						tmpRecord.parameter = part1 + " @@ " + part2;
					} else {
						splitResult = tmpRecord.parameter.split(":");
						if (splitResult.length >= 2)
							tmpRecord.parameter = splitResult[0] + ":" + argPreProcessPHF(splitResult[1]);
					}
				} else {
					String[] splitResult = tmpRecord.parameter.split(":");
					if (phfUselessSyscallList.contains(splitResult[1])) continue;
				}
			}
			resList.add(tmpRecord);
		}

		/**
		 *  remove duplicated sequences
		 */
		Filter filter = new Filter();
		// sequence = filter.filterUselessSyscalls(sequence,
		// uselessSyscallFilePath);
		resList = filter.filterConsecutiveBlocks(resList, minLengthOfConsecutiveBlock, maxLengthOfConsecutiveBlock,
				numberOfBlocksToKeep);
		return resList;
	}

	/**
	 * Argument pre-processing for phf.
	 * Basically consist of regular expression replacement.
	 * @param argumentString
	 * @return
	 */
	private static String argPreProcessPHF(String argumentString) {
		String tmpString = argumentString;
		while (tmpString.startsWith(" ") || tmpString.startsWith("\""))
			tmpString = tmpString.substring(1, tmpString.length());
		while (tmpString.endsWith(" ") || tmpString.endsWith("\""))
			tmpString = tmpString.substring(0, tmpString.length() - 1);
		String[] splitResults = tmpString.split("\\.");
		String suffix = "";
		if (splitResults.length >= 1)
			suffix = splitResults[splitResults.length - 1];
		else
			return "";
		String sysArg = null;
		if (configFile.contains(suffix)) {
			sysArg = "@ConfigurationFile";
		} else if (dataFile.contains(suffix)) {
			sysArg = "@DataFile";
		} else if (deviceFile.contains(suffix)) {
			sysArg = "@DeviceFile";
		} else if (excutableFile.contains(suffix)) {
			sysArg = "@ExecutableFile";
			for (String tmp : specialExeList)
				if (tmpString.endsWith(tmp)) {
					sysArg = tmp;
					break;
				}
		} else if (fontFile.contains(suffix)) {
			sysArg = "@FontFile";
		} else if (imageFile.contains(suffix)) {
			sysArg = "@ImageFile";
		} else if (languageFile.contains(suffix)) {
			sysArg = "@LanguageFile";
		} else if (libraryFile.contains(suffix)) {
			sysArg = "@LibraryFile";
		} else if (mediaFile.contains(suffix)) {
			sysArg = "@MediaFile";
		} else if (systemFile.contains(suffix)) {
			sysArg = "@TemporaryFile";
		} else if (webpageFile.contains(suffix)) {
			sysArg = "@WebpageFile";
		} else if (tmpString.endsWith("dll")) {
			// System.out.println(tmpString);
			sysArg = tmpString.split("\\\\")[tmpString.split("\\\\").length - 1];
		} else {
			sysArg = tmpString;
			sysArg = sysArg.replaceAll("[^\\\\]*\\.dat", "XXX.dat");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(temp|TMP|tmp)", "XXX.temp");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(nls|NLS)", "XXX.nls");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(png|PNG)", "XXX.png");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.css$", "XXX.css");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(js|JS)$", "XXX.js");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.json$", "XXX.json");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(htm|html)$", "XXX.htm");
			sysArg = sysArg.replaceAll("\\{.+-.+-.+-.+\\}", "XXX");
			sysArg = sysArg.replaceAll("[A-Z0-9]+-[^\\\\]+-[^\\\\]+-[0-9]+(_Classes)*", "XXX");
			sysArg = sysArg.replaceAll("Plane\\d+$", "Plane");
			sysArg = sysArg.replaceAll("msvideo\\d+$", "msvideo");
			sysArg = sysArg.replaceAll("wave\\d+$", "wave");
			sysArg = sysArg.replaceAll("midi\\d+$", "midi");
			sysArg = sysArg.replaceAll("HarddiskVolume\\d+", "HarddiskVolume");
			sysArg = sysArg.replaceAll("countrycode\\[\\d+\\]$", "countrycode");
			sysArg = sysArg.replaceAll("MonitorFunction\\d+$", "MonitorFunction");
			sysArg = sysArg.replaceAll("^#\\d+$", "#");
			sysArg = sysArg.replaceAll("Categories\\\\.*$", "Categories");
			sysArg = sysArg.replaceAll("Transforms\\\\[^\\\\]*?[0-9][^\\\\]*?$", "Transforms");
			sysArg = sysArg.replaceAll("UnitVersioning_\\d+$", "UnitVersioning");
			sysArg = sysArg.replaceAll("_[a-z0-9\\._]{30,}", "_XXX");
			sysArg = sysArg.replaceAll("[a-z0-9]{25,}", "XXX");
			sysArg = sysArg.replaceAll("(_\\d+){4,}", "");
			sysArg = sysArg.replaceAll("flag[A-Z0-9]{15,}", "flag");
			sysArg = sysArg.replaceAll("(\\\\[a-f0-9]+){2,}", "");
			sysArg = sysArg.replaceAll("([a-f0-9]+-){3,}[a-f0-9]+", "XXX");
			sysArg = sysArg.replaceAll("0x[0-9A-F]{7,}", "XXX");
			sysArg = sysArg.replaceAll("[a-f0-9A-F]{10,}", "XXX");
			sysArg = sysArg.replaceAll("(\\d+\\.){2,}\\d+", "XXX");
			sysArg = sysArg.replaceAll("\\d{7,}", "");
			sysArg = sysArg.replaceAll("XXX,\\d+", "XXX");
			sysArg = sysArg.split("\\\\")[sysArg.split("\\\\").length - 1];
			if (sysArg.equals("Mohammad") || sysArg.equals("cs"))
				sysArg = "LOCALUSERNAME";
		}
		return sysArg;
	}
	
	/**
	 * Below are several general names for all kinds of files used in phf pre-preprocessing.
	 */
	private static HashSet<String> configFile = new HashSet<String>() {
		{
			add("cnf");
			add("conf");
			add("config");
			add("ini");
			add("manifest");
		}
	};

	private static HashSet<String> dataFile = new HashSet<String>() {
		{
			add("bak");
			add("aff");
			add("bk");
			add("bkl");
			add("cab");
			add("crx");
			add("dat");
			add("data");
			add("db");
			add("db-journal");
			add("db-mj48724A972");
			add("db-mjD176209F0");
			add("db-shm");
			add("db-wal");
			add("dmp");
			add("docx");
			add("dotm");
			add("hdmp");
			add("json");
			add("ldb");
			add("log");
			add("pdf");
			add("sdb");
			add("sqlite");
			add("sqlite3");
			add("sqlite-journal");
			add("txt");
			add("wps");
			add("xml");
			add("xml~");
			add("xpi");
			add("xrc");
			add("zip");
			add("etl");
		}
	};

	private static HashSet<String> deviceFile = new HashSet<String>() {
		{
			add("drv");
		}
	};

	private static HashSet<String> excutableFile = new HashSet<String>() {
		{
			add("bat");
			add("bin");
			add("exe");
			add("hta");
			add("lnk");
			add("msi");
			add("php");
			add("py");
			add("vbs");
		}
	};

	private static HashSet<String> fontFile = new HashSet<String>() {
		{
			add("eot");
			add("fon");
			add("ttc");
			add("ttf");
		}
	};

	private static HashSet<String> imageFile = new HashSet<String>() {
		{
			add("alias");
			add("bitmap");
			add("bmp");
			add("gif");
			add("icm");
			add("ico");
			add("jpg");
			add("png");
		}
	};

	private static HashSet<String> languageFile = new HashSet<String>() {
		{
			add("mui");
			add("nls");
		}
	};

	private static HashSet<String> libraryFile = new HashSet<String>() {
		{
			add("pyd");
			add("bpl");
			add("jar");
		}
	};

	private static HashSet<String> mediaFile = new HashSet<String>() {
		{
			add("wav");
			add("wma");
			add("wmdb");
			add("wmv");
			add("wpl");
		}
	};

	private static HashSet<String> systemFile = new HashSet<String>() {
		{
			add("library-ms");
			add("sys");
		}
	};

	private static HashSet<String> temporaryFile = new HashSet<String>() {
		{
			add("cache");
			add("temp");
			add("temp-tmp");
			add("tmp");
		}
	};

	private static HashSet<String> webpageFile = new HashSet<String>() {
		{
			add("ashx");
			add("aspx");
			add("css");
			add("dtd");
			add("htm");
			add("js");
			add("url");
		}
	};

	
	/**
	 * Overall method for phf trace pre-preprocessing
	 * It will eliminate init-useless system calls and apply init-related argument pre-processing.
	 * Because initialization detection is a token-based detection, it doesn't need consecutive section filtering.
	 * @param sequence
	 * @param onlineMode
	 * @return
	 * @throws IOException
	 */
	public static List<EventRecord_NormalTest> initTracePreProcess(List<EventRecord_NormalTest> sequence,
			boolean onlineMode) throws IOException {

		// preprocess arguments
		Iterator<EventRecord_NormalTest> i = sequence.iterator();
		List<EventRecord_NormalTest> resList = new ArrayList<EventRecord_NormalTest>();

		while (i.hasNext()) {
			EventRecord_NormalTest tmpRecord = new EventRecord_NormalTest(i.next());
			if (!onlineMode) {
				if (!tmpRecord.eventType.equals("PerfInfoSysClEnter")) {
					String[] splitResult;
					if (tmpRecord.eventType.equals("ProcessStart") || tmpRecord.eventType.equals("ProcessEnd")) {
						String[] splitResult2 = tmpRecord.parameter.split(" @@ ");
						splitResult = splitResult2[0].split(":");
						String part1;
						if (splitResult.length >= 2)
							part1 = splitResult[0] + ":" + argPreProcessInit(splitResult[1]);
						else
							part1 = splitResult[0] + ":";
						splitResult = splitResult2[1].split(":");
						String part2;
						if (splitResult.length >= 2)
							part2 = splitResult[0] + ":" + argPreProcessInit(splitResult[1]);
						else
							part2 = splitResult[0] + ":";
						tmpRecord.parameter = part1 + " @@ " + part2;
					} else {
						splitResult = tmpRecord.parameter.split(":");
						if (splitResult.length >= 2)
							tmpRecord.parameter = splitResult[0] + ":" + argPreProcessInit(splitResult[1]);
					}
				} else {
					String[] splitResult = tmpRecord.parameter.split(":");
					if (initUselessSyscallList.contains(splitResult[1])) continue;
				}
			}
			resList.add(tmpRecord);
		}
		return resList;
	}

	/**
	 * Argument pre-processing for initialization.
	 * Basically consist of regular expression replacement.
	 * Compared to pre-processing for phf, it remains more original argument parts.
	 * @param argumentString
	 * @return
	 */
	private static String argPreProcessInit(String argumentString) {
		String tmpString = argumentString;
		String sysArg = null;

		if (tmpString.endsWith(".exe") || tmpString.endsWith(".dll")) {
			sysArg = tmpString.split("\\\\")[tmpString.split("\\\\").length - 1];
		}

		if (tmpString.endsWith("cmd.exe")) {
			sysArg = "cmd.exe";
		} else {
			sysArg = tmpString;
			sysArg = sysArg.replaceAll("[^\\\\]*\\.dat", "XXX.dat");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(temp|TMP|tmp)", "XXX.temp");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(nls|NLS)", "XXX.nls");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(png|PNG)", "XXX.png");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.css$", "XXX.css");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(js|JS)$", "XXX.js");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.json$", "XXX.json");
			sysArg = sysArg.replaceAll("[^\\\\]*\\.(htm|html)$", "XXX.htm");
			sysArg = sysArg.replaceAll("\\{.+-.+-.+-.+\\}", "XXX");
			sysArg = sysArg.replaceAll("\\\\[^\\\\]+-[^\\\\]+-[^\\\\]+-[^\\\\]+\\\\", "\\\\\\\\");
			sysArg = sysArg.replaceAll("\\\\[^\\\\]+-[^\\\\]+-[^\\\\]+-[^\\\\]+$", "\\XXX");
			sysArg = sysArg.replaceAll("\\{.+-.+-.+-.+-.+\\}", "XXX");
			sysArg = sysArg.replaceAll("[A-Z0-9]+-[^\\\\]+-[^\\\\]+-[0-9]+(_Classes)*", "XXX");
			sysArg = sysArg.replaceAll("Plane\\d+$", "Plane");
			sysArg = sysArg.replaceAll("msvideo\\d+$", "msvideo");
			sysArg = sysArg.replaceAll("wave\\d+$", "wave");
			sysArg = sysArg.replaceAll("midi\\d+$", "midi");
			sysArg = sysArg.replaceAll("HarddiskVolume\\d+", "HarddiskVolume");
			sysArg = sysArg.replaceAll("countrycode\\[\\d+\\]$", "countrycode");
			sysArg = sysArg.replaceAll("MonitorFunction\\d+$", "MonitorFunction");
			sysArg = sysArg.replaceAll("^#\\d+$", "#");
			sysArg = sysArg.replaceAll("Categories\\\\.*$", "Categories");
			sysArg = sysArg.replaceAll("Transforms\\\\[^\\\\]*?[0-9][^\\\\]*?$", "Transforms");
			sysArg = sysArg.replaceAll("UnitVersioning_\\d+$", "UnitVersioning");
			sysArg = sysArg.replaceAll("_[a-z0-9\\._]{30,}", "_XXX");
			sysArg = sysArg.replaceAll("[a-z0-9]{25,}", "XXX");
			sysArg = sysArg.replaceAll("(_\\d+){4,}", "");
			sysArg = sysArg.replaceAll("flag[A-Z0-9]{15,}", "flag");
			sysArg = sysArg.replaceAll("(\\\\[a-f0-9]+){2,}", "");
			sysArg = sysArg.replaceAll("([a-f0-9]+-){3,}[a-f0-9]+", "XXX");
			sysArg = sysArg.replaceAll("0x[0-9A-F]{7,}", "XXX");
			sysArg = sysArg.replaceAll("[a-f0-9A-F]{10,}", "XXX");
			sysArg = sysArg.replaceAll("(\\d+\\.){2,}\\d+", "XXX");
			sysArg = sysArg.replaceAll("\\d{7,}", "");
			sysArg = sysArg.replaceAll("XXX,\\d+", "XXX");
			sysArg = sysArg.replaceAll("\\{.*XXX.*\\}", "");
            sysArg = sysArg.replaceAll("\\\\Mohammad", "\\\\LOCALUSERNAME");
            sysArg = sysArg.replaceAll("\\\\cs", "\\\\LOCALUSERNAME");
		}

		return sysArg;
	}

	/**
	 * Used in ETWTraceFileConsumer.java.
	 * Used for transfer a super original EventRecord class to EventRecord_NormalTest class
	 * Actually, it applies a useful Event filtering according to the useful Event list in importantList/usefulevent2.txt. It will return null if manipulated data record is not in the useful event list 
	 * @param tmpRecord
	 * @param tid
	 * @param pid
	 * @return
	 * @throws IOException
	 */
	public static EventRecord_NormalTest selectArgFields(EventRecord tmpRecord, String tid, String pid)
			throws IOException {

		// preprocess arguments
		EventRecord_NormalTest resRecord = null;
		String fieldArg = "", fieldArg1 = "", fieldArg2 = "";
		if (tmpRecord.eventName.equals("ALPCALPC-Send-Message")
				|| tmpRecord.eventName.equals("ALPCALPC-Receive-Message")) {
			fieldArg = tmpRecord.arguments.get("ProcessName");
			if (fieldArg == null)
				fieldArg = "";
			resRecord = new EventRecord_NormalTest(-1, "-1", tmpRecord.eventName, "-1", "-1", tid, pid,
					"ProcessName:" + fieldArg, -1, "-1");
		} else if (tmpRecord.eventName.equals("DiskIoWrite") || tmpRecord.eventName.equals("FileIoCleanup")
				|| tmpRecord.eventName.equals("FileIoClose") || tmpRecord.eventName.equals("FileIoCreate")
				|| tmpRecord.eventName.equals("FileIoDelete") || tmpRecord.eventName.equals("FileIoDirEnum")
				|| tmpRecord.eventName.equals("FileIoFileCreate") || tmpRecord.eventName.equals("FileIoFileDelete")
				|| tmpRecord.eventName.equals("FileIoQueryInfo") || tmpRecord.eventName.equals("FileIoRead")
				|| tmpRecord.eventName.equals("FileIoSetInfo") || tmpRecord.eventName.equals("FileIoWrite")
				|| tmpRecord.eventName.equals("ImageLoad") || tmpRecord.eventName.equals("ImageUnLoad")
				|| tmpRecord.eventName.equals("FileIoFSControl")) {
			if (tmpRecord.arguments.containsKey("OpenPath")) {
				fieldArg = tmpRecord.arguments.get("OpenPath");
			} else {
				fieldArg = tmpRecord.arguments.get("FileName");
			}
			if (fieldArg == null)
				fieldArg = "";
			resRecord = new EventRecord_NormalTest(-1, "-1", tmpRecord.eventName, "-1", "-1", tid, pid,
					"FileName:" + fieldArg, -1, "-1");
		} /*
			 * else if (tmpRecord.eventName.equals("DiskIoDrvMjFnCall")) {
			 * fieldArg = tmpRecord.arguments.get("MajorFunction"); fieldArg =
			 * argfieldPreProcess(fieldArg); resRecord = new
			 * EventRecordG(tmpRecord.eventName, "MajorFunction:" + fieldArg); }
			 * else if (tmpRecord.eventName.equals("TCP_Connect") ||
			 * tmpRecord.eventName.equals("TCP_Disconnect") ||
			 * tmpRecord.eventName.equals("TCP_Send") ||
			 * tmpRecord.eventName.equals("TCP_Rec")) { resRecord = new
			 * EventRecordG(tmpRecord.eventName, "None:None"); }
			 */ else if (tmpRecord.eventName.equals("RegistryCreate") || tmpRecord.eventName.equals("RegistryDelete")
				|| tmpRecord.eventName.equals("RegistryEnumerateKey")
				|| tmpRecord.eventName.equals("RegistryEnumerateValueKey")
				|| tmpRecord.eventName.equals("RegistryKCBCreate") || tmpRecord.eventName.equals("RegistryKCBDelete")
				|| tmpRecord.eventName.equals("RegistryOpen") || tmpRecord.eventName.equals("RegistryQuery")
				|| tmpRecord.eventName.equals("RegistryQueryValue")
				|| tmpRecord.eventName.equals("RegistrySetInformation")
				|| tmpRecord.eventName.equals("RegistrySetValue")
				|| tmpRecord.eventName.equals("RegistryDeleteValue")) {
			fieldArg = tmpRecord.arguments.get("KeyName");
			if (fieldArg == null)
				fieldArg = "";
			resRecord = new EventRecord_NormalTest(-1, "-1", tmpRecord.eventName, "-1", "-1", tid, pid,
					"KeyName:" + fieldArg, -1, "-1");
		} else if (tmpRecord.eventName.equals("ProcessStart") || tmpRecord.eventName.equals("ProcessEnd")) {
			fieldArg1 = tmpRecord.arguments.get("ImageFileName");
			fieldArg2 = tmpRecord.arguments.get("CommandLine");
			if (fieldArg1 == null)
				fieldArg1 = "";
			if (fieldArg2 == null)
				fieldArg2 = "";
			resRecord = new EventRecord_NormalTest(-1, "-1", tmpRecord.eventName, "-1", "-1", tid, pid,
					"ImageFileName:" + fieldArg1 + " @@ " + "CommandLine:" + fieldArg2, -1, "-1");
		} else if (tmpRecord.eventName.equals("PerfInfoSysClEnter")) {
			resRecord = new EventRecord_NormalTest(-1, "-1", tmpRecord.eventName, "-1", "-1", tid, pid,
					"SystemCall:" + tmpRecord.arguments.get("SystemCall"), -1, "-1");
		} else if (tmpRecord.eventName.equals("ALPCALPC-Unwait")
				|| tmpRecord.eventName.equals("ALPCALPC-Wait-For-Reply")) {
			resRecord = new EventRecord_NormalTest(-1, "-1", tmpRecord.eventName, "-1", "-1", tid, pid, "", -1, "-1");
		}

		return resRecord;
	}
}
