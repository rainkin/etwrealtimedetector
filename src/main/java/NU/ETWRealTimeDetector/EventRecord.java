package NU.ETWRealTimeDetector;



import java.util.Map;


public class EventRecord {
	
	public String eventName;
	public String threadID;
	public String processID;
	public Map<String, String> arguments;
	
	public EventRecord() {
		
	}
	
}
