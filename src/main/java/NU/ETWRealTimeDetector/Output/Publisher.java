package NU.ETWRealTimeDetector.Output;

public abstract class Publisher {

	public abstract void output(Result result);
}
