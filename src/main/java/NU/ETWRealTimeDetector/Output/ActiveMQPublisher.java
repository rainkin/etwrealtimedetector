/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package NU.ETWRealTimeDetector.Output;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.jms.*;

public class ActiveMQPublisher extends Publisher{

    protected String user;
    protected String password;
    protected String host;
    protected int port;
    protected String destination;
    protected Connection connection;
    
    public ActiveMQPublisher() throws IOException {
    	BufferedReader br = new BufferedReader(new FileReader("config.txt"));
		user = br.readLine();
		password = br.readLine();
		// host = "localhost";
		// host = "10.214.148.122";
		host = br.readLine();
		port = Integer.parseInt(br.readLine());
//		destination = br.readLine();
		destination = "output";
		br.close();
	}
    
	@Override
	public void output(Result result) {
		try {
			if (connection == null){
				ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://" + host + ":" + port);	        
				connection = factory.createConnection(user, password);
				connection.start();
			}	        
	        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	        Destination dest = new ActiveMQTopic(destination);
	        MessageProducer producer = session.createProducer(dest);
	        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
	        GsonBuilder gsonBuilder = new GsonBuilder();
	        gsonBuilder.serializeSpecialFloatingPointValues();
	        Gson gson = gsonBuilder.create();
	        producer.send(session.createTextMessage(gson.toJson(result)));
	        
		} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	
	
}