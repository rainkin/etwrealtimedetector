package NU.ETWRealTimeDetector.Output;

import java.util.HashMap;
import java.util.Map;

import NU.ETWRealTimeDetector.Match.WFADetector;

public class Result {
	
	public String pid;
	public String tid;
	public String pcid;
	public Map<String, Double> phf2Credit = new HashMap<String, Double>();
	public String newAlarmedPhf;
	
	@Override
	public String toString() {
		return "Result [pid=" + pid + ", tid=" + tid + ", pcid=" + pcid + ", phf2Credit=" + phf2Credit + ", newAlarmedPhf=" + newAlarmedPhf + "]";
	}

	
	
	
}
