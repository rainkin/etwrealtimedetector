package NU.ETWRealTimeDetector;

public class EventRecordG {
	public String name;
	public String parameter;
	
	public EventRecordG(String name, String parameter) {
		this.name = name;
		this.parameter = parameter;
	}
	
	@Override
	public String toString() {
		return name + (parameter.isEmpty() ? "" : " @ " + parameter);
	}

	@Override public boolean equals(Object anObject) {
		if (anObject instanceof EventRecordG) 
			if (((EventRecordG)anObject).name.equals(this.name) && ((EventRecordG)anObject).parameter.equals(this.parameter))
				return true;
		return false;
	}
	
	@Override public int hashCode() {
		if (parameter != null)
			return (41 * (name.hashCode() + 41) + parameter.hashCode());
		else
			return (41 * (name.hashCode() + 41));
	}
	
	public static EventRecordG stringToEventRecordG(String data) {
		String[] splitResults = data.split(" @ ");
		EventRecordG returnValue = new EventRecordG(splitResults[0], "");
		if (splitResults.length > 1) returnValue.parameter = splitResults[1];
		return returnValue;
	}
}
