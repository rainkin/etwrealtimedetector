package NU.ETWRealTimeDetector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class offer some filtering tool used in real-time detection.
 * For now it only contains consecutive block reduction.
 * And those code are actually provided by Runqing.
 * Filter class and Utils class are both serving as some urgent tools in real-time detection, 
 * but Filter class contains more "general" data-driven algorithm and Utils class contains more "specific" manipulation with human heuristics. 
 * @author ander
 *
 * @param <T>
 */
public class Filter<T> {
	
	/**
	 * This is a fast version of consecutive block reduction proposed by Runqing.
	 * It seems to have some differences compared to the version following this one.
	 * @param sequences
	 * @param maxLengthOfConsecutiveBlock
	 * @return
	 */
	public List<T> filterConsectiveBlocksFasterVersion(List<T> sequences, int maxLengthOfConsecutiveBlock){
		if (maxLengthOfConsecutiveBlock <= 0)
			return sequences;
		
		List<T> mergedSequences = new ArrayList<T>();
		Map<T, LinkedList<Integer>> item2mergedIndexs = new HashMap<T, LinkedList<Integer>>();
		int numberOfMergedItems = 0;
		
		for(int originalIndex = 0; originalIndex < sequences.size(); originalIndex++){
			T item = sequences.get(originalIndex);
			if (!item2mergedIndexs.containsKey(item)) {
				LinkedList<Integer> indexs = new LinkedList<Integer>();
				indexs.push(originalIndex - numberOfMergedItems);
				item2mergedIndexs.put(item, indexs);				
				mergedSequences.add(item);
			}
			else {
				boolean isMerged = false;
				mergedSequences.add(item);
				item2mergedIndexs.get(item).push(originalIndex - numberOfMergedItems);
				
				LinkedList<Integer> previousIndexsOfItem = item2mergedIndexs.get(item);
				Iterator lit = previousIndexsOfItem.descendingIterator();
				int last = previousIndexsOfItem.peekFirst();
				while(lit.hasNext()){
					int previousIndexs = (int) lit.next();
					if (previousIndexs == last)
						continue;
					
					int mergedOriginalIndex = originalIndex - numberOfMergedItems;
					int numberOfComparedSequences = mergedOriginalIndex - previousIndexs;
					if (numberOfComparedSequences > maxLengthOfConsecutiveBlock || previousIndexs + 1 < numberOfComparedSequences){
						continue;
					}
										
					List<T> firstSequence = mergedSequences.subList(previousIndexs - numberOfComparedSequences+1, previousIndexs + 1);
					List<T> secondSequence = mergedSequences.subList(previousIndexs + 1, mergedOriginalIndex+1);
					boolean isMatched = firstSequence.equals(secondSequence);
					
					if (isMatched){
						numberOfMergedItems += numberOfComparedSequences;
						for(int index = mergedOriginalIndex ; index >= previousIndexs + 1 ; index--){
							item2mergedIndexs.get(mergedSequences.get(index)).pop();
							mergedSequences.remove(index);
						}											
						isMerged = true;
						break;
					}
							
				}
				
			} 
				
		}
		
		return mergedSequences;
		
				
	}
   
   
	/**
    * remove consecutive and repeated blocks. e.g. a b a b x z -> a b x z (repeated and consecutive)  e.g. a b x a b -> a b x a b (repeated but not consecutive)
    * @param sequences
    * @param minLengthOfConsecutiveBlock the min length of the repeated and consecutive substring, which will be removed 
    * @param maxLengthOfConsecutivBlock the max length of the repeated and consecutive substring, which will be removed 
    * @return filtered sequences
    */
   public List<T> filterConsecutiveBlocks(List<T> sequences, int minLengthOfConsectiveBlock, int maxLengthOfConsecutiveBlock,
		   int numberOfBlocksToKeep){
      if (maxLengthOfConsecutiveBlock <= 0)
         return sequences;
      
      List<T> mergedSequences = new ArrayList<T>(sequences); 
      for (int i = minLengthOfConsectiveBlock; i < maxLengthOfConsecutiveBlock; i++){
         mergedSequences = mergeNLengthConsective(mergedSequences, i, numberOfBlocksToKeep);
      }
      return mergedSequences; 
   }
   
   
   public List<T> mergeNLengthConsective(List<T> sequences, int n, int numberOfBlocksToKeep){
      if (sequences.isEmpty() || n > sequences.size()/2 + 1 || n <= 0)
         return sequences;
      
      List<T> finalSequences = new ArrayList<T>(sequences);
      for (int i = 0; i < n; i++){         
         List<T> mergedSequences = new ArrayList<T>();
         mergedSequences.addAll(finalSequences.subList(0, i));
         List<T> mergedTmp = _mergeNLengthConsective(finalSequences.subList(i, finalSequences.size()), n, numberOfBlocksToKeep);         
         mergedSequences.addAll(mergedTmp);
         finalSequences = mergedSequences;
      }
      return finalSequences;
   }
   
   private List<T> _mergeNLengthConsective(List<T> sequences, int n, int numberOfBlocksToKeep){      
      if (sequences.isEmpty() || n > sequences.size()/2 + 1)
         return sequences;
      
      List<T> mergedSequences = new ArrayList<T>();
      int consecutiveBlockTime = 1; // the current number of consecutive and repeated blocks, for string aaa, consecutiveBlocks = 3
//	      System.out.println("=====merge=====" + " " + n);
      int i = 0;
      for (; i < sequences.size() + 1 - 2*n; i = i + n){
         if (!sequences.subList(i, i+n).equals(sequences.subList(i+n, i+2*n))){            
	        List<T> repeatedBlocks = sequences.subList(i, i+n);
			if (consecutiveBlockTime == 1){ // doesn't have repeated and consecutie blocks
				mergedSequences.addAll(repeatedBlocks);
			} else { // have at least one repeated and consecutive blcoks
				int realNumberOfBlocksToKeep = Math.min(consecutiveBlockTime, numberOfBlocksToKeep);            
	            for (int k = 0; k < realNumberOfBlocksToKeep; k++){ // note that numberOfBlocks could be 0 which will not keep any blocks
	            	mergedSequences.addAll(repeatedBlocks);
	            }
			}        	            
            consecutiveBlockTime = 1;
         }
         else {
        	 consecutiveBlockTime += 1;
//	            System.out.println(sequences.subList(i, i+n));
         }
      }
      
      if (consecutiveBlockTime == 1){ // n=3, abcdefzx, i = 3
    	  mergedSequences.addAll(sequences.subList(i, sequences.size()));
      } else { // n = 3, numberOfBlocksToKeep = 1, abcabcabcxx, i = 6, keep abcxx
	      List<T> repeatedBlocks = sequences.subList(i, i+n);
		  int realNumberOfBlocksToKeep = Math.min(consecutiveBlockTime, numberOfBlocksToKeep);            
          for (int k = 0; k < realNumberOfBlocksToKeep; k++){ // note that numberOfBlocks could be 0 which will not keep any blocks
        	  mergedSequences.addAll(repeatedBlocks);
          }			
          consecutiveBlockTime = 0;
          mergedSequences.addAll(sequences.subList(i+n, sequences.size()));
      }
//	      System.out.println("+++++size+++++ " + mergedSequences.size());
      return mergedSequences;
   }




}
