import json

if __name__ == "__main__":
    inFile = open("TraceOfETW_20170606101.txt","r");
    outFile = open("TraceOfETW_20170606101.csv","w");

    eachline = inFile.readline();
    while eachline:
        print eachline
        record = json.loads(eachline)
        output = '%s,%s,%s,\n'%(record['syscall'],record['pid'],record['parameter'])
        outFile.write(output)
        eachline = inFile.readline();

    inFile.close();
    outFile.close();
